package com.songoda.treeassist;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Item;

import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
public class TreeAssistSaplingSelfPlant implements Runnable {
    private Item drop;
    private final static Set<Item> items = new HashSet<>();

    public TreeAssistSaplingSelfPlant(TreeAssist instance, Item item) {
        drop = item;
        items.add(drop);

        int delay = instance.getConfig().getInt("Auto Plant Dropped Saplings.Delay (seconds)", 5);
        if (delay < 1)
            delay = 1;

        Bukkit.getScheduler().runTaskLater(instance, this, 20L * delay);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void run() {
        if (!items.contains(drop))
            return;

        Block block = drop.getLocation().getBlock();

        if ((block.getType() == Material.AIR || block.getType() == Material.SNOW) &&
                (block.getRelative(BlockFace.DOWN).getType() == Material.DIRT ||
                        block.getRelative(BlockFace.DOWN).getType() == Material.MYCELIUM ||
                        block.getRelative(BlockFace.DOWN).getType() == Material.GRASS)) {

            block.setType(drop.getItemStack().getType());
            drop.remove();
        }
    }

    public static void remove(Item item) {
        items.remove(item);
    }
}
