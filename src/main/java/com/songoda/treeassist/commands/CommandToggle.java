package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public class CommandToggle extends AbstractCommand {
    public CommandToggle() {
        super(new String[]{"treeassist.toggle"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_TOGGLE.parse());
            return;
        }

        if (args.length > 1) {

            if (args.length > 2) {
                if (Bukkit.getWorld(args[2]) == null) {
                    sender.sendMessage(Lang.ERROR_NOTFOUND_WORLD.parse(args[2]));
                    return;
                }

                if (!sender.hasPermission("treeassist.toggle.other")) {
                    sender.sendMessage(Lang.ERROR_PERMISSION_TOGGLE_OTHER.parse());
                    return;
                }

                if (Utils.plugin.toggleWorld(args[2], args[1])) {
                    sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_OTHER_WORLD_ON.parse(args[1], args[2]));
                } else {
                    sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_OTHER_WORLD_OFF.parse(args[1], args[2]));
                }
                return;
            }
            if (Bukkit.getWorld(args[1]) == null) {
                if (!sender.hasPermission("treeassist.toggle.other")) {
                    sender.sendMessage(Lang.ERROR_PERMISSION_TOGGLE_OTHER.parse());
                    return;
                }

                if (Utils.plugin.toggleGlobal(args[1])) {
                    sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_OTHER_ON.parse(args[1]));
                } else {
                    sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_OTHER_OFF.parse(args[1]));
                }
                return;
            }

            if (Utils.plugin.toggleWorld(args[1], sender.getName())) {
                sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_YOU_WORLD_ON.parse(args[1]));
            } else {
                sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_YOU_WORLD_OFF.parse(args[1]));
            }

            return;
        }

        if (Utils.plugin.toggleGlobal(sender.getName())) {
            sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_YOU_ON.parse());
        } else {
            sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_YOU_OFF.parse());
        }
    }

    @Override
    public List<String> getMain() {
        return Collections.singletonList("toggle");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!tg");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist toggle [player/world] {world}";
    }

    @Override
    public String getCommandDescription() {
        return "Toggle plugin usage for you/others.";
    }

    @Override
    public CommandTree<String> getSubs() {
        final CommandTree<String> result = new CommandTree<>(null);
        for (World world : Bukkit.getServer().getWorlds()) {
            result.define(new String[]{"{Player}", world.getName()});
            result.define(new String[]{world.getName()});
        }
        result.define(new String[]{"{Player}"});
        return result;
    }
}
