package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public class CommandNoReplace extends AbstractCommand {
    public CommandNoReplace() {
        super(new String[]{"treeassist.noreplace"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_NOREPLACE.parse());
            return;
        }
        int seconds = Utils.plugin.getConfig().getInt("Sapling Replant.Command Time Delay (Seconds)", 30);
        Utils.plugin.listener.noReplace(sender.getName(), seconds);
        sender.sendMessage(Lang.SUCCESSFUL_NOREPLACE.parse(String.valueOf(seconds)));
    }

    @Override
    public List<String> getMain() {
        return Collections.singletonList("noreplace");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!nr");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist noreplace";
    }

    @Override
    public String getCommandDescription() {
        return "Stop replacing saplings for some time.";
    }

    @Override
    public CommandTree<String> getSubs() {
        return new CommandTree<>(null);
    }
}
