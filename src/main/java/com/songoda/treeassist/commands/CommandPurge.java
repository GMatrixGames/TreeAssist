package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.blocklists.FlatFileBlockList;
import com.songoda.treeassist.core.Utils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public class CommandPurge extends AbstractCommand {
    public CommandPurge() {
        super(new String[]{"treeassist.purge"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_PURGE.parse());
            return;
        }
        if (!argCountValid(sender, args, new Integer[]{2}))
            return;

        if (Utils.plugin.blockList instanceof FlatFileBlockList) {
            FlatFileBlockList bl = (FlatFileBlockList) Utils.plugin.blockList;
            try {
                int days = Integer.parseInt(args[1]);
                int done = bl.purge(days);

                sender.sendMessage(Lang.SUCCESSFUL_PURGE_DAYS.parse(done, args[1]));
            } catch (NumberFormatException e) {
                if (args[1].equalsIgnoreCase("global")) {
                    int done = bl.purge();
                    sender.sendMessage(Lang.SUCCESSFUL_PURGE_GLOBAL.parse(done));
                } else {
                    int done = bl.purge(args[1]);
                    sender.sendMessage(Lang.SUCCESSFUL_PURGE_WORLD.parse(done, args[1]));
                }
            }
        } else {
            sender.sendMessage(Lang.ERROR_ONLY_TREEASSIST_BLOCKLIST.parse());
        }
    }

    @Override
    public List<String> getMain() {
        return Collections.singletonList("purge");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!p");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist purge [global/world/days] {days}";
    }

    @Override
    public String getCommandDescription() {
        return "Purge entries for worlds/days.";
    }

    @Override
    public CommandTree<String> getSubs() {
        final CommandTree<String> result = new CommandTree<>(null);
        for (World world : Bukkit.getServer().getWorlds()) {
            result.define(new String[]{world.getName()});
        }
        result.define(new String[]{"global"});
        return result;
    }
}
