package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CommandTool extends AbstractCommand {
    public CommandTool() {
        super(new String[]{"treeassist.tool"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_TOGGLE_TOOL.parse());
            return;

        }
        if (sender instanceof Player) {
            Player player = (Player) sender;
            boolean found = false;
            for (ItemStack item : player.getInventory().getContents()) {
                if (item != null && item.hasItemMeta() && Utils.plugin.listener.isProtectTool(item)) {
                    player.getInventory().removeItem(item);
                    sender.sendMessage(Lang.SUCCESSFUL_TOOL_OFF.parse());
                    found = true;
                    break;
                }
            }
            if (!found) {
                player.getInventory().addItem(Utils.plugin.listener.getProtectionTool());
                sender.sendMessage(Lang.SUCCESSFUL_TOOL_ON.parse());
            }
            return;
        }
        sender.sendMessage(Lang.ERROR_ONLY_PLAYERS.parse());
    }

    @Override
    public List<String> getMain() {
        return Arrays.asList("commandtool", "tool");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!t");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist tool";
    }

    @Override
    public String getCommandDescription() {
        return "Toggle the sapling protection tool.";
    }

    @Override
    public CommandTree<String> getSubs() {
        return new CommandTree<>(null);
    }
}
