package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public class CommandReload extends AbstractCommand {
    public CommandReload() {
        super(new String[]{"treeassist.reload"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_RELOAD.parse());
            return;
        }
        Utils.plugin.blockList.save(true);
        Utils.plugin.reloadConfig();
        Utils.plugin.reloadLists();
        sender.sendMessage(Lang.SUCCESSFUL_RELOAD.parse());
    }

    @Override
    public List<String> getMain() {
        return Collections.singletonList("reload");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!rl");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist reload";
    }

    @Override
    public String getCommandDescription() {
        return "Reload the plugin.";
    }

    @Override
    public CommandTree<String> getSubs() {
        return new CommandTree<>(null);
    }
}
