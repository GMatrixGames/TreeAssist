package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.Collections;
import java.util.List;

public class CommandForceBreak extends AbstractCommand {
    public CommandForceBreak() {
        super(new String[]{"treeassist.forcebreak"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_FORCEBREAK.parse());
            return;
        }
        if (sender instanceof Player) {
            final Player player = (Player) sender;

            if (Utils.plugin.getConfig().getBoolean(
                    "Tools.Tree Destruction Require Tools")) {
                if (!Utils.isRequiredTool(player.getItemInHand())) {
                    sender.sendMessage(Lang.ERROR_NOT_TOOL.parse());
                    return;
                }
            }

            int radius = Utils.plugin.getConfig().getInt("Main.Force Break Default Radius", 10);

            if (args.length > 1) {
                try {
                    radius = Math.max(1, Integer.parseInt(args[1]));
                    int configValue = Utils.plugin.getConfig().getInt("Main.Force Break Max Radius", 30);
                    if (radius > configValue) {
                        sender.sendMessage(Lang.ERROR_OUT_OF_RANGE.parse(configValue));
                    }
                } catch (Exception ignore) {
                }
            }

            Utils.plugin.setCoolDownOverride(player.getName(), true);

            for (int x = -radius; x <= radius; x++) {
                for (int y = -radius; y <= radius; y++) {
                    nextBlock:
                    for (int z = -radius; z <= radius; z++) {
                        Block b = player.getLocation().add(x, y, z).getBlock();
                        if (b.getType().name().contains("_LOG")) {
                            if (b.getRelative(BlockFace.DOWN).getType() == Material.DIRT ||
                                    b.getRelative(BlockFace.DOWN).getType() == Material.GRASS ||
                                    b.getRelative(BlockFace.DOWN).getType() == Material.SAND) {
                                BlockBreakEvent bbe = new BlockBreakEvent(b, player);
                                Utils.plugin.getServer().getPluginManager().callEvent(bbe);
                            }
                        }
                    }
                }
            }

            Bukkit.getScheduler().runTaskLater(Utils.plugin, () -> Utils.plugin.setCoolDownOverride(player.getName(), false), Math.min(10, Utils.plugin.getConfig().getInt("Automatic Tree Destruction.Initial Delay (seconds)", 10) + 10) * 20);

            return;
        }
        sender.sendMessage(Lang.ERROR_ONLY_PLAYERS.parse());
    }

    @Override
    public List<String> getMain() {
        return Collections.singletonList("forcebreak");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!fb");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist forcebreak";
    }

    @Override
    public String getCommandDescription() {
        return "Force break trees around you.";
    }

    @Override
    public CommandTree<String> getSubs() {
        return new CommandTree<>(null);
    }
}
