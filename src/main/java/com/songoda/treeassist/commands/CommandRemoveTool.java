package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class CommandRemoveTool extends AbstractCommand {
    public CommandRemoveTool() {
        super(new String[]{"treeassist.removetool"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_REMOVETOOL.parse());
            return;

        }
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Utils.removeRequiredTool(player);
            return;
        }
        sender.sendMessage(Lang.ERROR_ONLY_PLAYERS.parse());
    }

    @Override
    public List<String> getMain() {
        return Collections.singletonList("removetool");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!rt");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist removetool";
    }

    @Override
    public String getCommandDescription() {
        return "Remove a required tool.";
    }

    @Override
    public CommandTree<String> getSubs() {
        return new CommandTree<>(null);
    }
}
