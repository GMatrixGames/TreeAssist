package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public class CommandGlobal extends AbstractCommand {
    public CommandGlobal() {
        super(new String[]{"treeassist.toggle.global"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_TOGGLE_GLOBAL.parse());
            return;
        }
        if (!Utils.plugin.Enabled) {
            Utils.plugin.Enabled = true;
            sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_GLOBAL_ON.parse());
        } else {
            Utils.plugin.Enabled = false;
            sender.sendMessage(Lang.SUCCESSFUL_TOGGLE_GLOBAL_OFF.parse());
        }
    }

    @Override
    public List<String> getMain() {
        return Collections.singletonList("global");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!g");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist global";
    }

    @Override
    public String getCommandDescription() {
        return "Toggle global plugin availability.";
    }

    @Override
    public CommandTree<String> getSubs() {
        return new CommandTree<>(null);
    }

}
