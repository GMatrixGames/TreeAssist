package com.songoda.treeassist.commands;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Sapling;

import java.util.Collections;
import java.util.List;

public class CommandForceGrow extends AbstractCommand {
    public CommandForceGrow() {
        super(new String[]{"treeassist.forcegrow"});
    }

    @Override
    public void commit(CommandSender sender, String[] args) {
        if (!hasPerms(sender)) {
            sender.sendMessage(Lang.ERROR_PERMISSION_FORCEGROW.parse());
            return;
        }
        if (sender instanceof Player) {
            Player player = (Player) sender;

            int radius = Utils.plugin.getConfig().getInt("Main.Force Grow Default Radius", 10);

            if (args.length > 1) {
                try {
                    radius = Math.max(1, Integer.parseInt(args[1]));
                    int configValue = Utils.plugin.getConfig().getInt("Main.Force Grow Max Radius", 30);
                    if (radius > configValue) {
                        sender.sendMessage(Lang.ERROR_OUT_OF_RANGE.parse(String.valueOf(configValue)));
                    }
                } catch (Exception ignore) {
                }
            }

            for (int x = -radius; x <= radius; x++) {
                for (int y = -radius; y <= radius; y++) {
                    nextBlock:
                    for (int z = -radius; z <= radius; z++) {
                        if (player.getLocation().add(x, y, z).getBlock().getType().name().contains("SAPLING")) {
                            Block block = player.getLocation().add(x, y, z).getBlock();
                            BlockState state = block.getState();
                            MaterialData data = state.getData();
                            Sapling sap = (Sapling) data;

                            TreeType type = TreeType.TREE;

                            if (sap.getSpecies() != TreeSpecies.GENERIC) {
                                type = TreeType.valueOf(sap.getSpecies().name());
                            }
                            if (type == TreeType.JUNGLE) {
                                type = TreeType.SMALL_JUNGLE;
                            }

                            for (int offset = 0; offset < 7; offset++) {
                                if (block.getRelative(BlockFace.UP, offset).getType() == Material.DIRT) {
                                    continue nextBlock;
                                }
                            }

                            block.setType(Material.AIR);
                            for (int i = 0; i < 20; i++) {
                                if (block.getWorld().generateTree(block.getLocation(), type)) {
                                    continue nextBlock;
                                }
                            }
                            block.setType(sap.getItemType());
                        }
                    }
                }
            }

            return;
        }
        sender.sendMessage(Lang.ERROR_ONLY_PLAYERS.parse());
    }

    @Override
    public List<String> getMain() {
        return Collections.singletonList("forcegrow");
    }

    @Override
    public String getName() {
        return getClass().getName();
    }

    @Override
    public List<String> getShort() {
        return Collections.singletonList("!fg");
    }

    @Override
    public String getCommandSyntax() {
        return "treeassist forcegrow";
    }

    @Override
    public String getCommandDescription() {
        return "Force saplings around you to grow.";
    }

    @Override
    public CommandTree<String> getSubs() {
        return new CommandTree<>(null);
    }
}
