package com.songoda.treeassist;

import org.bukkit.Location;

public class TreeAssistProtect implements Runnable {
    public final TreeAssist plugin;
    private Location location;

    @SuppressWarnings("WeakerAccess")
    public TreeAssistProtect(TreeAssist instance, Location importLocation) {
        this.plugin = instance;
        this.location = importLocation;
    }

    @Override
    public void run() {
        if (plugin.isEnabled())
            plugin.saplingLocationList.remove(this.location);
    }
}
