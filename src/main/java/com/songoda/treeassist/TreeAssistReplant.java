package com.songoda.treeassist;

import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;

import java.util.List;

public class TreeAssistReplant implements Runnable {
    public final TreeAssist plugin;
    private byte data;
    private List<Block> bottomBlocks;
    private TreeSpecies species;

    @SuppressWarnings("WeakerAccess")
    public TreeAssistReplant(TreeAssist instance, TreeSpecies species, List<Block> bottomBlocks) {
        this.plugin = instance;
        this.species = species;
        this.data = -1;
        this.bottomBlocks = bottomBlocks;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void run() {
        for (Block block : this.bottomBlocks) {
            if (!plugin.isEnabled()) continue;
            block.setType(Material.OAK_SAPLING);
            BlockState state = block.getState();
            org.bukkit.material.Sapling sap = (org.bukkit.material.Sapling) state.getData();
            sap.setSpecies(species);
            state.setData(sap);
            state.update();
            block.setBlockData(state.getBlockData());
            if (plugin.getConfig().getInt("Time to Block Sapling Growth (Seconds)") > 0)
                plugin.getListener().getAntiGrow().add(block, plugin.getConfig().getInt("Time to Block Sapling Growth (Seconds)"));
        }
    }
}
