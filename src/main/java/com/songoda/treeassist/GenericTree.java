package com.songoda.treeassist;

import com.songoda.arconix.plugin.Arconix;
import com.songoda.treeassist.core.Utils;
import com.songoda.treeassist.events.TATreeBrokenEvent;
import com.songoda.treeassist.hooks.HookHandler;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Leaves;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Mushroom;
import org.bukkit.material.Tree;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

@SuppressWarnings({"WeakerAccess", "deprecation", "Duplicates"})
public class GenericTree {
    protected enum TreeType {
        OAK, SPRUCE, BIRCH, JUNGLE, BROWN_SHROOM, RED_SHROOM, CUSTOM, ACACIA, DARK_OAK, THIN_JUNGLE
    }

    protected boolean valid = false;
    protected List<Block> removeBlocks = new ArrayList<>();
    protected List<Block> totalBlocks = new ArrayList<>();

    protected TreeSpecies species = null;
    protected String mushSpecies = "RED";
    protected String speciesString;

    protected Block bottom;
    protected Block top;

    protected List<Block> bottomBlocks = new ArrayList<>();

    protected boolean fastDecaying = false;

    private void checkAndDoSaplingProtect(BlockBreakEvent event) {
        Block b = event.getBlock();
        Player p = event.getPlayer();

        Material blockMat = b.getType();
        if (!blockMat.name().contains("_LOG")) {
            if (blockMat.name().contains("SAPLING")) {
                if (Utils.plugin.getConfig().getBoolean("Sapling Replant.Block all breaking of Saplings")) {
                    p.sendMessage(Lang.INFO_NEVER_BREAK_SAPLINGS.parse());
                    event.setCancelled(true);
                } else if (Utils.plugin.saplingLocationList.contains(b.getLocation())) {
                    if (p.getGameMode() == GameMode.CREATIVE) {
                        Utils.plugin.saplingLocationList.remove(b.getLocation());
                        return;
                    }
                    p.sendMessage(Lang.INFO_SAPLING_PROTECTED.parse());
                    event.setCancelled(true);
                }
            } else if (blockMat == Material.GRASS || blockMat == Material.DIRT
                    || blockMat == Material.CLAY || blockMat == Material.SAND) {
                if (Utils.plugin.saplingLocationList.contains(b
                        .getRelative(BlockFace.UP, 1).getLocation())) {
                    if (p.getGameMode() == GameMode.CREATIVE) {
                        Utils.plugin.saplingLocationList.remove(b.getRelative(BlockFace.UP, 1).getLocation());
                        return;
                    }

                    p.sendMessage(Lang.INFO_SAPLING_PROTECTED.parse());
                    event.setCancelled(true);
                }
            } else {
                Utils.plugin.saplingLocationList.remove(b.getLocation());
            }
        }
    }

    protected List<Block> calculate(final Block bottom, final Block top) {
        //Bukkit.getPlayer("Songoda").sendMessage("still gotta do this ;D");
        return new ArrayList<>();
    }

    public void beginSearch(BlockBreakEvent event) {
        calculate(event);
        checkAndDoSaplingProtect(event);

    }

    private void calculate(BlockBreakEvent event) {
        Block b = event.getBlock();
        Player p = event.getPlayer();


        if (b.getState().getData() instanceof Tree || b.getType() == Material.BROWN_MUSHROOM || b.getType() == Material.RED_MUSHROOM) {

            if (b.getState().getData() instanceof Tree)
                species = ((Tree) b.getState().getData()).getSpecies();
            else if (b.getType().equals(Material.BROWN_MUSHROOM)) {
                mushSpecies = "BROWN";
                speciesString = "Brown Shroom";
            } else if (b.getType().equals(Material.BROWN_MUSHROOM)) {
                mushSpecies = "RED";
                speciesString = "Red Shroom";
            }

            if (b.getState().getData() instanceof Tree) {
                speciesString = species.name().substring(0, 1).toUpperCase() + species.name().substring(1).toLowerCase();
                if (speciesString.equals("Generic"))
                    speciesString = "Oak";
                if (speciesString.equals("Redwood"))
                    speciesString = "Spruce";
                if (speciesString.equals("Dark_oak"))
                    speciesString = "Dark Oak";
            }

            bottom = b;
            top = getTop(b);

            if (!TreeAssist.getInstance().getConfig().getBoolean("Main.Ignore User Placed Blocks")) {
                if (TreeAssist.getInstance().blockList.isPlayerPlaced(b)) {
                    TreeAssist.getInstance().blockList.removeBlock(b);
                    TreeAssist.getInstance().blockList.save();
                    return; // This is a placed block and thus will be ignored.
                }
            }

            if (!hasPerms(p))
                return;

            if (TreeAssist.getInstance().getConfig().getBoolean("Sapling Replant.Enforce"))
                maybeReplant(event, p, b);

            if (TreeAssist.getInstance().isForceAutoDestroy()) {
                findYourBlocks(b);
                removeLater();
            }

            final String lore = Arconix.pl().getApi().format().formatText(TreeAssist.getInstance().getConfig().getString("Automatic Tree Destruction.Required Lore", ""));

            if (!"".equals(lore)) {
                ItemStack item = p.getItemInHand();
                if (item == null || !item.hasItemMeta() || !item.getItemMeta().hasLore() || !item.getItemMeta().getLore().contains(lore)) {
                    return;
                }
            }

            if ((!TreeAssist.getInstance().getConfig().getBoolean("Automatic Tree Destruction.When Sneaking") && event.getPlayer().isSneaking()) ||
                    (!TreeAssist.getInstance().getConfig().getBoolean("Automatic Tree Destruction.When Not Sneaking") && !event.getPlayer().isSneaking())) {
                return;
            }

            if (Utils.plugin.hasCoolDown(p)) {
                p.sendMessage(Lang.INFO_COOLDOWN_VALUE.parse(Utils.plugin.getCoolDown(p)));
                return;
            }

            if (HookHandler.mcMmoHook.treeFeller(p)) {
                maybeReplant(event, p, b);
                return;
            }

            boolean onlyAbove = TreeAssist.getInstance().getConfig().getBoolean("Main.Destroy Only Blocks Above");

            if (!onlyAbove)
                bottom = getBottom(b);

            if (top == null && !TreeAssist.getInstance().isEWGEnabled || bottom == null && !TreeAssist.getInstance().isEWGEnabled)
                return;

            if (TreeAssist.getInstance().getConfig().getBoolean("Main.Automatic Tree Destruction") && !TreeAssist.getInstance().isEWGEnabled) {
                if (top == null)
                    return; // not a valid tree

                if (top.getY() - bottom.getY() < 3)
                    return; // not a valid tree
            }

            boolean success = false;
            boolean damage = false;

            if (!event.isCancelled() && TreeAssist.getInstance().getConfig().getBoolean("Main.Automatic Tree Destruction")) {
                if (TreeAssist.getInstance().getConfig().getBoolean("Tools.Tree Destruction Require Tools")) {
                    if (!Utils.isRequiredTool(p.getItemInHand()))
                        return;
                }

                //Commenting out in case somehow something breaks from removing this - Nova
                /*
                for (BlockFace face : Utils.NEIGHBORFACES) {
                    if (!Utils.naturalMaterials.contains(b.getRelative(face).getType())) {
                        TreeType type = getTreeTypeByBlock(b.getRelative(face));
                        if (type == TreeType.DARK_OAK || type == TreeType.JUNGLE || type == TreeType.SPRUCE) {
                            continue;
                        }
                    }
                }
                */

                if (!TreeAssist.getInstance().isDisabled(p.getWorld().getName(), p.getName())) {
                    success = willBeDestroyed();
                    damage = TreeAssist.getInstance().getConfig().getBoolean("Main.Apply Full Tool Damage");
                }
            }
            if (success) {
                HashSet<Block> allBlocks = new HashSet<>();
                HashSet<Block> logBlocks = new HashSet<>();
                logBlocks.add(b);

                HashSet<Block> treeBlocks = new HashSet<>();

                int searchSquareSize = 3;
                if (b.getState().getData() instanceof Tree) {
                    if (top.getY() - bottom.getY() >= 10)
                        searchSquareSize++;
                }

                int neg = -2;
                int pos = 2;

                if (TreeAssist.getInstance().isEWGEnabled) {
                    searchSquareSize = 45;
                    neg = -3;
                    pos = 3;
                }

                while (logBlocks.iterator().hasNext()) {
                    Block b3 = logBlocks.iterator().next();
                    for (int x = neg; x <= pos; ++x) {
                        for (int y = neg; y <= (pos + 2); ++y) {
                            if (!onlyAbove || (y + b.getY()) >= b.getY()) {
                                for (int z = neg; z <= pos; ++z) {
                                    if (x != 0 || y != 0 || z != 0) {

                                        Block b2 = b3.getRelative(x, y, z);

                                        if (b2.getX() > b.getX() + searchSquareSize || b2.getX() < b.getX() - searchSquareSize || b2.getZ() > b.getZ() + searchSquareSize || b2.getZ() < b.getZ() - searchSquareSize)
                                            break;

                                        if (!treeBlocks.contains(b2)) {
                                            if ((b2.getType().name().contains("LEAVES") && !(b.getState().getData() instanceof Mushroom)) && TreeAssist.getInstance().getConfig().getBoolean("Automatic Tree Destruction.Remove Leaves", true))
                                                treeBlocks.add(b2);
                                            if (b2.getType().equals(Material.BROWN_MUSHROOM) && (b.getState().getData() instanceof Mushroom) || b2.getType().equals(Material.RED_MUSHROOM) && (b.getState().getData() instanceof Mushroom))
                                                treeBlocks.add(b2);
                                        }

                                        if (b2.getType().name().contains("_LOG") && !(b.getState().getData() instanceof Mushroom) ||
                                                b2.getType() == Material.RED_MUSHROOM && (b.getState().getData() instanceof Mushroom) ||
                                                b2.getType() == Material.BROWN_MUSHROOM && (b.getState().getData() instanceof Mushroom)) {
                                            if (!allBlocks.contains(b2)) {
                                                if (b2.getY() == b.getY())
                                                    this.bottomBlocks.add(b2);
                                                treeBlocks.add(b2);
                                                logBlocks.add(b2);
                                                allBlocks.add(b2);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    logBlocks.remove(b3);
                }

                maybeReplant(event, p, b);

                final ItemStack tool = (damage && p.getGameMode() != GameMode.CREATIVE) ? p.getItemInHand() : null;

                for (Block block : treeBlocks) {
                    if (block.getType() != Material.AIR) {
                        TATreeBrokenEvent e = new TATreeBrokenEvent(block, p, tool);
                        TreeAssist.getInstance().getServer().getPluginManager().callEvent(e);
                        if (!event.isCancelled()) {
                            TreeAssist.getInstance().blockList.logBreak(block, p);
                            if ((block.getType().name().contains("_LOG")) && TreeAssist.getInstance().getConfig().getBoolean("Main.Auto Add To Inventory", false)) {
                                p.getInventory().addItem(block.getState().getData().toItemStack(1));
                                block.setType(Material.AIR);
                            } else {
                                if (Utils.plugin.getConfig().getBoolean("Automatic Tree Destruction.Animated")) {
                                    Utils.plugin.getListener().animateLogChop(block, tool, p, top.getY() - bottom.getY(), speciesString);
                                } else {
                                    if (tool != null) {
                                        breakBlock(block, tool, p, speciesString);
                                        if (tool.getType().getMaxDurability() > 0 && tool.getDurability() == tool.getType().getMaxDurability()) {
                                            p.getInventory().remove(tool);
                                            break;
                                        }
                                    } else
                                        block.breakNaturally();
                                }
                            }
                        }
                    }

                }

            }
        }
    }

    private GenericTree maybeReplant(BlockBreakEvent event, Player player, Block block) {
        if (!(block.getState().getData() instanceof Mushroom)) {
            if (getTop(event.getBlock()) == null)
                return null;

            if (!isBottom(block) && TreeAssist.getInstance().getConfig().getBoolean("Sapling Replant.Bottom Block has to be Broken First")) {
                return null;
            } else {
                block = bottom;
                if (block == null)
                    return null;
            }

            Material below = block.getRelative(BlockFace.DOWN).getType();
            if (!(below == Material.DIRT || below == Material.GRASS || below == Material.CLAY || below == Material.SAND || below == Material.MYCELIUM))
                return this;

            //!TreeAssist.getInstance().getListener().isNoReplace(player.getName()) <-- Removed because its hella broken!
            if (TreeAssist.getInstance().getConfig().getBoolean("Main.Sapling Replant") && !event.isCancelled() && (willReplant())) {

                if (!TreeAssist.getInstance().getConfig().getBoolean("Main.Use Permissions") || player.hasPermission("treeassist.replant")) {
                    if (TreeAssist.getInstance().getConfig().getBoolean("Tools.Sapling Replant Require Tools")) {
                        if (!Utils.isRequiredTool(player.getItemInHand())) {
                            if (TreeAssist.getInstance().isForceAutoDestroy()) {
                                findYourBlocks(block);
                                if (isValid())
                                    removeLater();
                                return this;
                            }
                            return null;
                        }
                    }
                    int delay = TreeAssist.getInstance().getConfig().getInt("Sapling Replant.Delay until Sapling is replanted (seconds) (minimum 1 second)");
                    if (delay < 1) {
                        delay = 1;
                    }

                    if (!TreeAssist.getInstance().getConfig().getBoolean("Sapling Replant.Bottom Block has to be Broken First")) {
                        bottomBlocks.remove(bottom);
                        bottom = getTrueBottom(block);
                        bottomBlocks.add(bottom);
                        handleSaplingReplace(delay);
                    } else if (isBottom(block)) {
                        // block is bottom
                        handleSaplingReplace(delay);
                    }
                }
            }
        }
        return null;
    }

    protected boolean isBottom(Block block) {
        return block.equals(bottom);
    }

    protected Block getBottom(Block block) {
        int counter = 1;
        do {
            //if (block.getRelative(0, 0 - counter, 0).getType() == logMaterial) {
            if (block.getRelative(0, 0 - counter, 0).getState().getData() instanceof Tree ||
                    block.getRelative(0, 0 - counter, 0).getState().getData() instanceof Mushroom) {
                counter++;
            } else {

                bottom = block.getRelative(0, 1 - counter, 0);
                if (bottom.getRelative(BlockFace.DOWN).getType() != Material.DIRT &&
                        bottom.getRelative(BlockFace.DOWN).getType() != Material.GRASS &&
                        bottom.getRelative(BlockFace.DOWN).getType() != Material.CLAY &&
                        bottom.getRelative(BlockFace.DOWN).getType() != Material.SAND) {
                    return null;
                }
                return bottom;
            }
        } while (block.getY() - counter > 0);

        bottom = null;
        return null;
    }

    protected Block getTrueBottom(Block block) {
        boolean hasBottom = false;
        Block current = block;
        while (!hasBottom) {
            Material t = current.getType();
            if (t == Material.DIRT || t == Material.GRASS || t == Material.SAND || t == Material.CLAY) {
                hasBottom = true;
            } else {
                current = current.getRelative(BlockFace.DOWN);
            }
        }
        return current.getRelative(BlockFace.UP);
    }

    protected Block getTop(Block block) {
        int maxY = block.getWorld().getMaxHeight() + 25;
        int counter = 1;

        while (block.getY() + counter < maxY) {
            //if (block.getRelative(0, counter, 0).getType() == leafMaterial) {
            if (block.getRelative(0, counter, 0).getState().getData() instanceof Leaves ||
                    block.getRelative(0, counter, 0).getState().getData() instanceof Mushroom) {
                top = block.getRelative(0, counter - 1, 0);
            }
            counter++;
        }

        if (top != null && top.getState().getData() instanceof Mushroom)
            top = top.getRelative(0, 1, 0);

        return (top != null && leafCheck(top)) ? top.getRelative(0, 1, 0) : null;
    }

    protected void handleSaplingReplace(int delay) {
        if (bottom == null)
            return;

        removeBlocks.remove(bottom);
        totalBlocks.remove(bottom);

        if (TreeAssist.getInstance().isEWGEnabled) {
            this.bottomBlocks = new ArrayList<>();
            bottomBlocks.add(bottom);
        }

        Runnable b = new TreeAssistReplant(Utils.plugin, species, bottomBlocks);
        Utils.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Utils.plugin, b, 20 * delay);

        int procTime = Utils.plugin.getConfig().getInt("Sapling Replant.Time to Protect Sapling (Seconds)");
        if (procTime > 0) {
            Utils.plugin.saplingLocationList.add(bottom.getLocation());
            Runnable X = new TreeAssistProtect(Utils.plugin, bottom.getLocation());

            Utils.plugin.getServer().getScheduler().scheduleSyncDelayedTask(Utils.plugin, X, 20 * procTime);
        }
    }

    protected boolean hasPerms(Player player) {
        if (!Utils.plugin.getConfig().getBoolean("Main.Use Permissions")) {
            return true;
        }
        switch (speciesString) {
            case "Dark Oak":
                return player.hasPermission("treeassist.destroy.darkoak");
            case "Red Shroom":
                return player.hasPermission("treeassist.destroy.redshroom");
            case "Brown Shroom":
                return player.hasPermission("treeassist.destroy.brownshroom");
            default:
                return player.hasPermission("treeassist.destroy." + speciesString);
        }
    }

    protected static boolean isLeaf(Block block) {
        return block.getState().getData() instanceof Leaves || block.getState().getData() instanceof Mushroom;
    }

    protected static boolean isLeaf(MaterialData matData) {
        return matData instanceof Leaves || matData instanceof Mushroom;
    }

    protected boolean willBeDestroyed() {
        return Utils.plugin.getConfig().getBoolean("Automatic Tree Destruction.Tree Types." + speciesString);
    }

    protected boolean willReplant() {
        return Utils.replantType(species);
    }

    public boolean isValid() {
        return true;
    }

    /**
     * Break a block and apply damage to the tool
     *
     * @param block  the block to break
     * @param tool   the item held to break with
     * @param player the breaking player
     */
    public static void breakBlock(final Block block, final ItemStack tool, final Player player, final String species) {

        if ((tool != null) && (tool.getDurability() > tool.getType().getMaxDurability())) return;

        TATreeBrokenEvent event = new TATreeBrokenEvent(block, player, tool);
        Utils.plugin.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) return;

        boolean leaf = isLeaf(block);


        Tree tree = null;

        if (block.getState().getData() instanceof Tree)
            tree = (Tree) block.getState().getData();

        //Stop from breaking random blocks it shouldn't
        if (!leaf && tree == null) {
            //System.out.println("BLOCK SHOULDN'T BREAK: " + block.getType().name());
            return;
        }

        if (!leaf && HookHandler.mcMMO && player != null)
            HookHandler.mcMmoHook.addExp(player, block, species);

        if (!leaf && HookHandler.jobsReborn && player != null)
            HookHandler.jobsHook.addExp(player, block, species);

        int chance = 100;

        if (tool != null && !leaf) {
            chance = Utils.plugin.getConfig().getInt("Tools.Drop Chance." + tool.getType().name(), 100);
            if (chance < 1)
                chance = 1;
        }

        if (chance > 99 || (new Random()).nextInt(100) < chance) {
            Utils.plugin.blockList.logBreak(block, player);
            if (player != null && (block.getType().name().contains("_LOG"))
                    && Utils.plugin.getConfig().getBoolean("Main.Auto Add To Inventory", false)) {
                player.getInventory().addItem(block.getState().getData().toItemStack(1));
                block.setType(Material.AIR);
            } else {
                if (tool != null && tool.hasItemMeta() && tool.getItemMeta().getEnchants().containsKey(Enchantment.SILK_TOUCH)
                        && block.getType().toString().startsWith("HUGE_MUSH")) {
                    Material mat = block.getType();
                    byte b = block.getData();
                    block.setType(Material.AIR);
                    block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(mat, 1, b));
                } else {
                    block.breakNaturally(tool);
                }
            }
            if (player != null)
                player.sendBlockChange(block.getLocation(), Material.AIR, (byte) 0);

            if (leaf) {
                ConfigurationSection cs = Utils.plugin.getConfig().getConfigurationSection("Custom Drops");

                for (String key : cs.getKeys(false)) {
                    int customChance = (int) (cs.getDouble(key, 0.0d) * 100000d);

                    if ((new Random()).nextInt(100000) < customChance) {
                        if (key.equalsIgnoreCase("LEAVES") && tree != null) {
                            Leaves leaves = new Leaves();
                            leaves.setSpecies(tree.getSpecies());
                            block.getWorld().dropItemNaturally(block.getLocation(), leaves.toItemStack(1));
                        } else {
                            try {
                                Material mat = Material.valueOf(key.toUpperCase());
                                block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(mat));
                            } catch (Exception e) {
                                Utils.plugin.getLogger().warning("Invalid config value: Custom Drops." + key + " is not a valid Material!");
                            }
                        }
                    }
                }
            }
        } else {
            block.setType(Material.AIR);
            if (player != null)
                player.sendBlockChange(block.getLocation(), Material.AIR, (byte) 0);
        }

        if (!leaf && tool != null && player != null) {
            if (tool.containsEnchantment(Enchantment.DURABILITY)) {
                int damageChance = (int) (100d / ((double) tool.getEnchantmentLevel(Enchantment.DURABILITY) + 1d));

                int random = new Random().nextInt(100);

                if (random >= damageChance)
                    return; // nodamage -> out!
            }

            int ench = 100;

            if (tool.getEnchantments().containsKey(Enchantment.DURABILITY))
                ench = 100 / (tool.getEnchantmentLevel(Enchantment.DURABILITY) + 1);

            if ((new Random()).nextInt(100) > ench)
                return; // no damage

            if (tool.getType().name().contains("_AXE")) {
                tool.setDurability((short) (tool.getDurability() + 1));
            } else if (tool.getType() != Material.AIR) { //ToDO: May need to specifically call out tools
                tool.setDurability((short) (tool.getDurability() + 2));
            }
        }
    }

    public static void breakBlockRewrite(final Block block, final Material mat, final MaterialData matData, byte data, final ItemStack tool, final Player player, final boolean breakPhysical, String species) {

        if ((tool != null) && (tool.getDurability() > tool.getType().getMaxDurability())) return;

        TATreeBrokenEvent event = new TATreeBrokenEvent(block, player, tool);
        Utils.plugin.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled()) return;

        boolean leaf = isLeaf(matData);


        Tree tree = null;

        if (matData instanceof Tree)
            tree = (Tree) matData;

        //Stop from breaking random blocks it shouldn't
        if (!leaf && tree == null) {
            //System.out.println("BLOCK SHOULDN'T BREAK: " + block.getType().name());
            return;
        }

        if (!leaf && HookHandler.mcMMO && player != null)
            HookHandler.mcMmoHook.addExp(player, matData, species);

        if (!leaf && HookHandler.jobsReborn && player != null) {
            HookHandler.jobsHook.addExp(player, matData, species);
        }

        int chance = 100;

        if (tool != null && !leaf) {
            chance = Utils.plugin.getConfig().getInt("Tools.Drop Chance." + tool.getType().name(), 100);
            if (chance < 1)
                chance = 1;
        }

        if (chance > 99 || (new Random()).nextInt(100) < chance) {
            Utils.plugin.blockList.logBreak(block, player);
            if (player != null && mat.name().contains("_LOG")
                    && Utils.plugin.getConfig().getBoolean("Main.Auto Add To Inventory", false)) {
                player.getInventory().addItem(matData.toItemStack(1));
                if (breakPhysical)
                    block.setType(Material.AIR);
            } else {
                if (tool != null && tool.hasItemMeta() && tool.getItemMeta().getEnchants().containsKey(Enchantment.SILK_TOUCH)
                        && mat.toString().startsWith("HUGE_MUSH")) {
                    if (breakPhysical)
                        block.setType(Material.AIR);
                    block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(mat, 1, data));
                } else {
                    if (breakPhysical)
                        block.breakNaturally(); //ToDO: May need to include tool to apply damage.
                }
            }
            if (player != null && breakPhysical)
                player.sendBlockChange(block.getLocation(), Material.AIR, (byte) 0);

            //noinspection Duplicates
            if (leaf) {
                ConfigurationSection cs = Utils.plugin.getConfig().getConfigurationSection("Custom Drops");

                for (String key : cs.getKeys(false)) {
                    int customChance = (int) (cs.getDouble(key, 0.0d) * 100000d);

                    if ((new Random()).nextInt(100000) < customChance) {
                        if (key.equalsIgnoreCase("LEAVES") && tree != null) {
                            Leaves leaves = new Leaves();
                            leaves.setSpecies(tree.getSpecies());
                            block.getWorld().dropItemNaturally(block.getLocation(), leaves.toItemStack());
                        } else {
                            try {
                                Material newMat = Material.valueOf(key.toUpperCase());
                                block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(newMat));
                            } catch (Exception e) {
                                Utils.plugin.getLogger().warning("Invalid config value: Custom Drops." + key + " is not a valid Material!");
                            }
                        }
                    }
                }
            }
        } else {
            if (breakPhysical) {
                block.setType(Material.AIR);
                if (player != null)
                    player.sendBlockChange(block.getLocation(), Material.AIR, (byte) 0);
            }
        }

        //noinspection Duplicates
        if (!leaf && tool != null && player != null) {
            if (tool.containsEnchantment(Enchantment.DURABILITY)) {
                int damageChance = (int) (100d / ((double) tool.getEnchantmentLevel(Enchantment.DURABILITY) + 1d));

                int random = new Random().nextInt(100);

                if (random >= damageChance)
                    return; // nodamage -> out!
            }

            int ench = 100;

            if (tool.getEnchantments().containsKey(Enchantment.DURABILITY))
                ench = 100 / (tool.getEnchantmentLevel(Enchantment.DURABILITY) + 1);

            if ((new Random()).nextInt(100) > ench)
                return; // no damage


            if (tool.getType().name().contains("_AXE")) {
                tool.setDurability((short) (tool.getDurability() + 1));
            } else if (tool.getType() != Material.AIR) { //ToDO: May need to specifically call out tools
                tool.setDurability((short) (tool.getDurability() + 2));
            }
        }
    }


    protected void findYourBlocks(Block block) {
        bottom = getBottom(block);
        top = getTop(block);

        totalBlocks = new ArrayList<>();

        if (bottom == null) {
            removeBlocks = new ArrayList<>();
            return;
        }
        if (top == null) {
            removeBlocks = new ArrayList<>();
            return;
        }
        valid = true;
    }

    protected boolean leafCheck(final Block block) {
        int total = 0;

        for (int x = -2; x < 3; x++) {
            for (int z = -2; z < 3; z++) {
                for (int y = -1; y < 1; y++) {
                    if (isLeaf(block.getRelative(x, y, z)))
                        total++;
                }
            }
        }

        return total > 3;
    }

    protected void removeLater() {
        removeBlocks = calculate(bottom, top);
        //System.out.println("1:"+this.debugCount);

        final int delay = Utils.plugin.getConfig().getBoolean("Main.Initial Delay") ? Utils.plugin.getConfig().getInt("Automatic Tree Destruction.Initial Delay (seconds)") * 20 : 0;
        final int offset = Utils.plugin.getConfig().getInt("Automatic Tree Destruction.Delay (ticks)");

        class RemoveRunner extends BukkitRunnable {
            private final GenericTree me;

            RemoveRunner(GenericTree tree) {
                me = tree;
            }

            @Override
            public void run() {
                for (Block block : removeBlocks) {
                    if (block.getType().name().contains("SAPLING") || block.getType() == Material.BROWN_MUSHROOM || block.getType() == Material.RED_MUSHROOM) {

                    } else if (!Utils.plugin.getConfig().getBoolean("Automatic Tree Destruction.Remove Leaves") && isLeaf(block)) {

                    } else {
                        if (!fastDecaying && isLeaf(block)) {
                            Utils.plugin.getListener().breakRadiusLeaves(block);
                            fastDecaying = true;
                        }
                        TATreeBrokenEvent event = new TATreeBrokenEvent(block, null, null);
                        Utils.plugin.getServer().getPluginManager().callEvent(event);
                        if (!event.isCancelled()) {
                            Utils.plugin.blockList.logBreak(block, null);
                            block.breakNaturally();
                        }
                    }
                    removeBlocks.remove(block);
                    return;

                }
                me.valid = false;
                try {
                    this.cancel();
                } catch (Exception ignore) {
                }
            }

        }

        (new RemoveRunner(this)).runTaskTimer(Utils.plugin, delay, offset + 1);
    }

    public boolean contains(Block block) {

        List<Block> myRemoveBlocks = new ArrayList<>(removeBlocks);

        Iterator<Block> i = myRemoveBlocks.iterator();
        try {
            while (i.hasNext()) {

                Block b = i.next();
                if (block.getType() == Material.AIR ||
                        block.getType().name().contains("SAPLING") || block.getType() == Material.BROWN_MUSHROOM
                        || block.getType() == Material.RED_MUSHROOM) {
                    removeBlocks.remove(b);
                }
            }
        } catch (ConcurrentModificationException ignore) {

        }
        myRemoveBlocks = new ArrayList<>(totalBlocks);

        i = myRemoveBlocks.iterator();
        while (i.hasNext()) {

            Block b = i.next();
            if (block.getType() == Material.AIR ||
                    block.getType().name().contains("SAPLING") || block.getType() == Material.BROWN_MUSHROOM
                    || block.getType() == Material.RED_MUSHROOM) {
                totalBlocks.remove(b);
            }
        }
        if (removeBlocks.size() < 1 && totalBlocks.size() < 1) {
            this.valid = false;
            return false;
        }


        return removeBlocks.contains(block) || totalBlocks.contains(block);
    }

    /**
     * thanks to filbert66 for this determination method!
     *
     * @param tool the itemstack being used
     * @return the seconds that it will take to destroy
     */
    public int calculateCooldown(ItemStack tool) {

        Material element = (tool != null ? tool.getType() : null);

        float singleTime;

        if (element != null) {
            switch (element) {
                case GOLDEN_AXE:
                    singleTime = 0.25F;
                    break;
                case DIAMOND_AXE:
                    singleTime = 0.4F;
                    break;
                case IRON_AXE:
                    singleTime = 0.5F;
                    break;
                case STONE_AXE:
                    singleTime = 0.75F;
                    break;
                case WOODEN_AXE:
                    singleTime = 1.5F;
                    break;
                default:
                    singleTime = 3.0F;
                    break;
            }
        } else {
            singleTime = 3.0F;
        }

        float efficiencyFactor = 1.0F;
        if (tool != null && tool.hasItemMeta()) {
            int efficiencyLevel = tool.getItemMeta().getEnchantLevel(
                    Enchantment.DIG_SPEED);
            for (int i = 0; i < efficiencyLevel; i++) {
                efficiencyFactor /= 1.3F;
            }
        }

        int numLogs = 0;
        for (Block b : removeBlocks) {
            if (isLeaf(b)) {
                numLogs++;
            }
        }

        return (int) (numLogs * singleTime * efficiencyFactor);
    }

}
