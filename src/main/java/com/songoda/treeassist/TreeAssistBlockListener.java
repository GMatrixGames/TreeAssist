package com.songoda.treeassist;

import com.songoda.arconix.plugin.Arconix;
import com.songoda.treeassist.core.Utils;
import com.songoda.treeassist.events.TALeafDecay;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Leaves;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Mushroom;
import org.bukkit.material.Tree;
import org.bukkit.util.Vector;

import java.util.*;

@SuppressWarnings({"WeakerAccess", "deprecation"})
public class TreeAssistBlockListener implements Listener {
    public TreeAssist plugin;
    private final Map<String, Long> noreplace = new HashMap<>();

    private final TreeAssistAntiGrow antiGrow;

    public TreeAssistBlockListener(TreeAssist instance) {
        plugin = instance;
        antiGrow = new TreeAssistAntiGrow(plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onLeavesDecay(LeavesDecayEvent event) {
        if (plugin.getConfig().getBoolean("Leaf Decay.Fast Leaf Decay") && plugin.Enabled) {
            Block block = event.getBlock();
            World world = block.getWorld();

            if (!plugin.isActive(world))
                return;

            //if (!Utils.plugin.getConfig().getBoolean("Automatic Tree Destruction.Animated"))
            breakRadiusLeaves(block);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onInteract(PlayerInteractEvent event) {
        if (event.hasItem() && event.hasBlock() && this.isProtectTool(event.getPlayer().getItemInHand())) {
            Block clicked = event.getClickedBlock();

            if (clicked.getType().name().contains("SAPLING") && plugin.saplingLocationList.contains(clicked.getLocation())) {
                plugin.saplingLocationList.remove(clicked.getLocation());
                event.getPlayer().sendMessage(Lang.SUCCESSFUL_PROTECT_OFF.parse());
            } else {
                plugin.saplingLocationList.add(clicked.getLocation());
                event.getPlayer().sendMessage(Lang.SUCCESSFUL_PROTECT_ON.parse());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!plugin.getConfig().getBoolean("Main.Ignore User Placed Blocks") && (event.getBlock().getType().name().contains("_LOG")) && plugin.getConfig().getBoolean("Worlds.Enable Per World") && !plugin.getConfig().getList("Worlds.Enabled Worlds").contains(event.getBlock().getWorld().getName())) {
            return;
        }

        Block block = event.getBlock();
        plugin.blockList.addBlock(block);
        plugin.blockList.save();
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockIgnite(BlockIgniteEvent event) {
        checkFire(event.getBlock());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBurn(BlockBurnEvent event) {
        checkFire(event.getBlock());
    }

    public boolean animateLogChop(Block block, ItemStack tool, Player player, int height, String species) {
        Random r = new Random();
        double v = 0.15 + (0.30 - 0.15) * r.nextDouble();

        double north = 0;
        double west = 0;

        if (player != null) {
            String d = Arconix.pl().getApi().getPlayer(player).getPlayerDirection();
            switch (d) {
                case "NORTH":
                    north = v;
                    break;
                case "SOUTH":
                    north = v * -1;
                    break;
                case "WEST":
                    west = v;
                    break;
                case "EAST":
                    west = v * -1;
                    break;
            }
        }

        FallingBlock sand;
        final Material savedType = block.getType();
        final byte savedData = block.getData();
        final MaterialData letsTry = block.getState().getData();

        if (TreeAssist.getInstance().v1_8)
            sand = block.getWorld().spawnFallingBlock(block.getLocation(), block.getState().getData());
        else
            sand = block.getWorld().spawnFallingBlock(block.getLocation(), block.getType(), block.getData());

        sand.setDropItem(false);
        sand.setHurtEntities(false);
        sand.setVelocity(new Vector(west + (double) (block.getY() - block.getY()) * 0.010D, (double) (block.getX() - block.getX()) * 0.005D, north));

        block.setType(Material.AIR);

        long wmax = 40L;
        long wmin = 25L;

        if (TreeAssist.getInstance().isEWGEnabled || height >= 10) {
            wmax = wmax + 20;
            wmin = wmin + 20;
        }

        Long vv = wmin + (long) (Math.random() * (wmax - wmin));
        Bukkit.getScheduler().scheduleSyncDelayedTask(TreeAssist.getInstance(), () -> {
            Block bb = sand.getLocation().getBlock();
            final Material currentType = bb.getType();
            final MaterialData currentMatData = bb.getState().getData();

            boolean breakBlock = false;
            boolean breakPhysical = false;

            if (letsTry instanceof Tree || letsTry instanceof Leaves || letsTry instanceof Mushroom) {
                breakBlock = true;
                if (currentMatData instanceof Tree || currentMatData instanceof Leaves || currentMatData instanceof Mushroom) {
                    breakPhysical = true;
                }
            }

            if (breakBlock) {
                GenericTree.breakBlockRewrite(bb, savedType, letsTry, savedData, tool, player, breakPhysical, species);
                if (currentType.name().contains("SAPLING")) {
                    bb.setType(currentType);
                }
            } else {
                bb.setType(currentType);
            }
        }, vv);

        return true;
    }

    private void checkFire(Block block) {

        if (plugin.getConfig().getBoolean("Sapling Replant.Replant When Tree Burns Down") && plugin.Enabled && plugin.getConfig().getBoolean("Worlds.Enable Per World") && !plugin.getConfig().getList("Worlds.Enabled Worlds").contains(block.getWorld().getName())) {
            return;
        }

        MaterialData data = block.getState().getData();
        if (data instanceof Tree) {
            Material logMat = block.getType();
            Tree tree = (Tree) data;
            Block onebelow = block.getRelative(BlockFace.DOWN, 1);
            Block oneabove = block.getRelative(BlockFace.UP, 1);
            if ((onebelow.getType() == Material.DIRT || onebelow.getType() == Material.GRASS) && (oneabove.getType() == Material.AIR || oneabove.getType() == logMat)) {
                List<Block> bottomBlocks = new ArrayList<>();
                bottomBlocks.add(block);
                Runnable b = new TreeAssistReplant(plugin, tree.getSpecies(), bottomBlocks);
                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, b, 20);
            }
        }
    }

    protected final static Set<GenericTree> trees = new HashSet<>();

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {

        if (!plugin.Enabled)
            return;

        Set<GenericTree> myTrees = new HashSet<>(trees);

        for (GenericTree tree : myTrees) {
            if (tree.contains(event.getBlock())) {
                return;
            } else if (!tree.isValid()) {
                trees.remove(tree);
            }
        }

        GenericTree agt = new GenericTree();
        agt.beginSearch(event);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!plugin.getConfig().getBoolean("Main.Toggle Default"))
            plugin.toggleGlobal(event.getPlayer().getName());
    }

    @EventHandler(ignoreCancelled = true)
    public void onStructureGrow(StructureGrowEvent event) {
        if (antiGrow.contains(event.getLocation()))
            event.setCancelled(true);
    }

    /**
     * Checks if the block is a leaf block and drops it
     * if no log is in 2 block radius around
     *
     * @param blockAt the block to check
     */
    private void breakIfLonelyLeaf(Block blockAt) {
        if (!blockAt.getType().name().contains("LEAVES")) {
            return;
        }
        World world = blockAt.getWorld();

        int fail = -1; // because we will fail once, when finding blockAt

        for (int x = blockAt.getX() - 2; x <= blockAt.getX() + 2; x++) {
            for (int y = blockAt.getY() - 2; y <= blockAt.getY() + 2; y++) {
                for (int z = blockAt.getZ() - 2; z <= blockAt.getZ() + 2; z++) {
                    fail += calcAir(world.getBlockAt(x, y, z));
                    if (fail > 4) {
                        return; // fail threshold -> out!
                    }
                }
            }
        }

        TALeafDecay event = new TALeafDecay(blockAt);
        Utils.plugin.getServer().getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            Utils.plugin.blockList.logBreak(blockAt, null);
            blockAt.breakNaturally();
        }
    }

    /**
     * enforces an 8 block radius FloatingLeaf removal
     *
     * @param blockAt the block to check
     */
    public void breakRadiusLeaves(Block blockAt) {
        TALeafDecay event = new TALeafDecay(blockAt);
        Utils.plugin.getServer().getPluginManager().callEvent(event);

        if (event.isCancelled())
            return;

        Utils.plugin.blockList.logBreak(blockAt, null);
        blockAt.breakNaturally();
        World world = blockAt.getWorld();
        int x = blockAt.getX();
        int y = blockAt.getY();
        int z = blockAt.getZ();
        for (int x2 = -8; x2 < 9; x2++) {
            for (int z2 = -8; z2 < 9; z2++) {
                breakIfLonelyLeaf(world.getBlockAt(x + x2, y + 2, z + z2));
                breakIfLonelyLeaf(world.getBlockAt(x + x2, y + 1, z + z2));
                breakIfLonelyLeaf(world.getBlockAt(x + x2, y, z + z2));
                breakIfLonelyLeaf(world.getBlockAt(x + x2, y - 1, z + z2));
                breakIfLonelyLeaf(world.getBlockAt(x + x2, y - 2, z + z2));
            }
        }
    }

    private int calcAir(Block blockAt) {
        if (blockAt.getType() == Material.AIR || blockAt.getType() == Material.VINE || blockAt.getType().name().contains("LEAVES")) {
            return 0;
        } else if (blockAt.getType().name() == "_LOG") {
            return 5;
        } else {
            return 1;
        }
    }

    private final String displayName = "" + ChatColor.GREEN + ChatColor.ITALIC + "TreeAssist Protect";

    public boolean isProtectTool(ItemStack item) {
        return item != null && item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().equals(displayName);
    }

    public TreeAssistAntiGrow getAntiGrow() {
        return antiGrow;
    }

    public ItemStack getProtectionTool() {
        ItemStack item = new ItemStack(Material.GOLDEN_HOE);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayName);
        item.setItemMeta(meta);
        return item;
    }

    public void noReplace(String name, int seconds) {
        noreplace.put(name, System.currentTimeMillis() / 1000 + seconds);
    }

    public boolean isNoReplace(String name) {
        if (noreplace.containsKey(name) && noreplace.get(name) < System.currentTimeMillis() / 1000) {
            noreplace.remove(name);
            return false;
        }
        return true;
    }
}