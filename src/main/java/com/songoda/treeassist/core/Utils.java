package com.songoda.treeassist.core;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.TreeAssist;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Utils {
    private Utils() {
    }

    // if it's not one of these blocks, it's safe to assume its a house/building
    public static List<Material> naturalMaterials = new ArrayList<>();

    static {
        // elements
        naturalMaterials.add(Material.AIR);
        naturalMaterials.add(Material.FIRE);
        naturalMaterials.add(Material.WATER);
        naturalMaterials.add(Material.WATER);
        naturalMaterials.add(Material.SNOW_BLOCK);
        naturalMaterials.add(Material.SNOW);

        // ground materials

        naturalMaterials.add(Material.STONE);
        naturalMaterials.add(Material.GRASS);
        naturalMaterials.add(Material.DIRT);
        naturalMaterials.add(Material.SAND);
        naturalMaterials.add(Material.CLAY);
        naturalMaterials.add(Material.MYCELIUM);

        // natural growing things

        naturalMaterials.add(Material.SPRUCE_SAPLING);
        naturalMaterials.add(Material.ACACIA_SAPLING);
        naturalMaterials.add(Material.BIRCH_SAPLING);
        naturalMaterials.add(Material.DARK_OAK_SAPLING);
        naturalMaterials.add(Material.JUNGLE_SAPLING);
        naturalMaterials.add(Material.OAK_SAPLING);

        naturalMaterials.add(Material.ACACIA_LEAVES);
        naturalMaterials.add(Material.BIRCH_LEAVES);
        naturalMaterials.add(Material.DARK_OAK_LEAVES);
        naturalMaterials.add(Material.JUNGLE_LEAVES);
        naturalMaterials.add(Material.SPRUCE_LEAVES);

        naturalMaterials.add(Material.DANDELION);
        naturalMaterials.add(Material.DANDELION_YELLOW);
        naturalMaterials.add(Material.POPPY);
        naturalMaterials.add(Material.SUNFLOWER);
        naturalMaterials.add(Material.BROWN_MUSHROOM);
        naturalMaterials.add(Material.RED_MUSHROOM);
        naturalMaterials.add(Material.TALL_GRASS);
        naturalMaterials.add(Material.DEAD_BUSH);
        naturalMaterials.add(Material.SUGAR_CANE);
        naturalMaterials.add(Material.VINE);
        naturalMaterials.add(Material.LILY_PAD);
        naturalMaterials.add(Material.RED_MUSHROOM);
        naturalMaterials.add(Material.BROWN_MUSHROOM);
        naturalMaterials.add(Material.MELON);
        naturalMaterials.add(Material.PUMPKIN);
        naturalMaterials.add(Material.COCOA);

        // blocks that are used in farms

        naturalMaterials.add(Material.TORCH);
        naturalMaterials.add(Material.RAIL);
    }

    public static void removeRequiredTool(Player player) {
        ItemStack inHand = player.getItemInHand();
        if (inHand == null || inHand.getType() == Material.AIR) {
            player.sendMessage(Lang.ERROR_EMPTY_HAND.parse());
            return;
        }

        String definition = null;

        List<?> fromConfig = Utils.plugin.getConfig().getList("Tools.Tools List");
        if (fromConfig.contains(inHand.getType().name())) {
            fromConfig.remove(inHand.getType().name());
            definition = inHand.getType().name();
        } else if (fromConfig.contains(inHand.getType())) {
            fromConfig.remove(fromConfig.contains(inHand.getType()));
            definition = String.valueOf(fromConfig.contains(inHand.getType()));
        } else if (fromConfig.contains(String.valueOf(inHand.getType()))) {
            fromConfig.remove(String.valueOf(fromConfig.contains(inHand.getType())));
            definition = String.valueOf(fromConfig.contains(inHand.getType()));
        } else {
            for (Object obj : fromConfig) {
                if (!(obj instanceof String)) {
                    continue; // skip item IDs
                }
                String tool = (String) obj;
                if (!tool.startsWith(inHand.getType().name())) {
                    continue; // skip other names
                }

                String[] values = tool.split(":");

                if (values.length < 2) {
                    definition = tool;
                    // (found) name
                } else {

                    for (Enchantment ench : inHand.getEnchantments().keySet()) {
                        if (!ench.getName().equalsIgnoreCase(values[1])) {
                            continue; // skip other enchantments
                        }
                        int level = 0;
                        if (values.length < 3) { // has correct enchantment, no level needed
                            definition = tool;
                        } else {
                            try {
                                level = Integer.parseInt(values[2]);
                            } catch (Exception e) { // invalid level defined, defaulting to no
                                definition = tool;
                                // level
                            }

                            if (level > inHand.getEnchantments().get(ench)) {
                                continue; // enchantment too low
                            }
                            definition = tool;
                        }

                    }
                }
            }
            if (definition == null) {
                player.sendMessage(Lang.ERROR_REMOVETOOL_NOTDONE.parse());
                return;
            } else {
                fromConfig.remove(definition);
            }
        }

        player.sendMessage(Lang.SUCCESSFUL_REMOVETOOL.parse(definition));


    }

    public static void addRequiredTool(Player player) {
        ItemStack item = player.getItemInHand();
        if (item == null || item.getType() == Material.AIR) {
            player.sendMessage(Lang.ERROR_EMPTY_HAND.parse());
            return;
        }
        if (isRequiredTool(item)) {
            player.sendMessage(Lang.ERROR_ADDTOOL_ALREADY.parse());
            return;
        }
        StringBuilder entry = new StringBuilder();

        try {
            entry.append(item.getType().name());
        } catch (Exception e) {
            final String msg = "Could not retrieve item type name: " + String.valueOf(item.getType());
            plugin.getLogger().severe(msg);
            player.sendMessage(Lang.ERROR_ADDTOOL_OTHER.parse(msg));
            return;
        }

        boolean found = false;

        for (Enchantment ench : item.getEnchantments().keySet()) {
            if (found) {
                player.sendMessage(Lang.WARNING_ADDTOOL_ONLYONE.parse(ench.getName()));
                break;
            }
            entry.append(':');
            entry.append(ench.getName());
            entry.append(':');
            entry.append(item.getEnchantmentLevel(ench));
            found = true;
        }
        List<String> result = new ArrayList<>();
        List<?> fromConfig = Utils.plugin.getConfig().getList("Tools.Tools List");
        for (Object obj : fromConfig) {
            if (obj instanceof String) {
                result.add(String.valueOf(obj));
            }
        }
        result.add(entry.toString());
        Utils.plugin.getConfig().set("Tools.Tools List", result);
        Utils.plugin.saveConfig();
        player.sendMessage(Lang.SUCCESSFUL_ADDTOOL.parse(entry.toString()));
    }

    /**
     * Check if the player has a needed tool
     *
     * @param inHand the held item
     * @return if the player has a needed tool
     */
    public static boolean isRequiredTool(final ItemStack inHand) {
        List<?> fromConfig = Utils.plugin.getConfig().getList("Tools.Tools List");
        if (fromConfig.contains(inHand.getType().name())) {
            return true;
        }

        for (Object obj : fromConfig) {
            if (!(obj instanceof String)) {
                continue; // skip item IDs
            }
            String tool = (String) obj;
            if (!tool.startsWith(inHand.getType().name())) {
                continue; // skip other names
            }

            String[] values = tool.split(":");

            if (values.length < 2) {
                return true; // no enchantment found, defaulting to plain
                // (found) name
            }

            for (Enchantment ench : inHand.getEnchantments().keySet()) {
                if (!ench.getName().equalsIgnoreCase(values[1])) {
                    continue; // skip other enchantments
                }
                int level = 0;
                if (values.length < 3) {
                    return true; // has correct enchantment, no level needed
                }
                try {
                    level = Integer.parseInt(values[2]);
                } catch (Exception e) {
                    return true; // invalid level defined, defaulting to no
                    // level
                }

                if (level > inHand.getEnchantments().get(ench)) {
                    continue; // enchantment too low
                }
                return true;
            }
        }

        return false;
    }

    public static String joinArray(final Object[] array, final String glue) {
        final StringBuilder result = new StringBuilder();
        for (final Object o : array) {
            result.append(glue);
            result.append(o);
        }
        if (result.length() <= glue.length()) {
            return result.toString();
        }
        return result.substring(glue.length());
    }


    /**
     * Should the given species be replanted?
     *
     * @param species the tree species
     * @return if a sapling should be replanted
     */
    public static boolean replantType(TreeSpecies species) {
        if (species == TreeSpecies.GENERIC)
            return Utils.plugin.getConfig().getBoolean("Sapling Replant.Tree Types to Replant.Oak");
        if (species == TreeSpecies.REDWOOD)
            return Utils.plugin.getConfig().getBoolean("Sapling Replant.Tree Types to Replant.Spruce");
        if (species == TreeSpecies.BIRCH)
            return Utils.plugin.getConfig().getBoolean("Sapling Replant.Tree Types to Replant.Birch");
        if (species == TreeSpecies.JUNGLE)
            return Utils.plugin.getConfig().getBoolean("Sapling Replant.Tree Types to Replant.Jungle");
        if (species == TreeSpecies.ACACIA)
            return Utils.plugin.getConfig().getBoolean("Sapling Replant.Tree Types to Replant.Acacia");
        if (species == TreeSpecies.DARK_OAK)
            return Utils.plugin.getConfig().getBoolean("Sapling Replant.Tree Types to Replant.Dark Oak");

        return false;
    }


    public static void initiateList(String string, List<Integer> validTypes) {
        for (Object obj : Utils.plugin.getConfig().getList(string)) {
            if (obj instanceof Integer) {
                validTypes.add((Integer) obj);
                continue;
            }
            if (obj.equals("LIST ITEMS GO HERE")) {
                List<Object> list = new ArrayList<>();
                list.add(-1);
                Utils.plugin.getConfig().set(string, list);
                Utils.plugin.saveConfig();
                break;
            }
            validTypes.add(Integer.parseInt(((String) obj).split(":")[0]));
        }
    }

    public static TreeAssist plugin;

    public static int versionCompare(String theirs, String ours) {
        String[] aTheirs = theirs.split(".");
        String[] aOurs = ours.split(("."));
        int i = 0;
        while (i < aOurs.length) {
            if (aTheirs.length <= i)
                return -1;

            try {
                int iTheirs = Integer.parseInt(aTheirs[i], 36);
                int iOurs = Integer.parseInt(aOurs[i], 36);

                if (iTheirs != iOurs) {
                    return iTheirs - iOurs;
                }
            } catch (Exception e) {
                return 0; // something fancy, assume special version that should be a snapshot
            }
            i++;
        }
        return 0;
    }
}
