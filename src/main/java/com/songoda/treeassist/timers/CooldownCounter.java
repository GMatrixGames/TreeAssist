package com.songoda.treeassist.timers;

import com.songoda.treeassist.Lang;
import com.songoda.treeassist.core.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class CooldownCounter extends BukkitRunnable {
    private final String name;
    private int seconds;

    public CooldownCounter(Player player, int seconds) {

        name = player.getName();
        this.seconds = seconds;
    }

    @Override
    public void run() {
        if (--seconds <= 0) {
            commit();
            try {
                this.cancel();
            } catch (IllegalStateException ignore) {
            }
        }
    }

    private void commit() {
        Utils.plugin.removeCountDown(name);
        Player p = Bukkit.getPlayer(name);

        if (p != null)
            p.sendMessage(Lang.INFO_COOLDOWN_DONE.parse());
    }

    public int getSeconds() {
        return seconds;
    }

}
