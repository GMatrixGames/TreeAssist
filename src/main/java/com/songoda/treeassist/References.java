package com.songoda.treeassist;

class References {

    private String prefix;

    References() {
        prefix = Lang.INFO_PREFIX.parse() + " ";
    }

    String getPrefix() {
        return this.prefix;
    }
}
