package com.songoda.treeassist.hooks;

import com.gmail.nossr50.api.AbilityAPI;
import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.config.experience.ExperienceConfig;
import com.gmail.nossr50.datatypes.skills.SkillType;
import com.songoda.treeassist.core.Utils;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Tree;
import org.bukkit.plugin.Plugin;

public class MCMMOHook implements IHook {

    /**
     * Add mcMMO exp for destroying a block
     *
     * @param player the player to give exp
     * @param block  the block being destroyed
     */
    @Override
    public void addExp(Player player, Block block, String species) {
        this.addExp(player, block.getState().getData(), species);
    }

    @Override
    public void addExp(Player player, MaterialData matData, String species) {
        if (HookHandler.mcMMO) {
            Plugin mcmmo = Utils.plugin.getServer().getPluginManager().getPlugin("mcMMO");

            if (player == null)
                return;

            if (!(matData instanceof Tree))
                return;

            Tree tree = (Tree) matData;
            int toAdd = ExperienceConfig.getInstance().getXp(SkillType.WOODCUTTING, tree);
            if (player.isOnline()) {
                ExperienceAPI.addXP(player, "Woodcutting", toAdd);
            } else {
                ExperienceAPI.addRawXPOffline(player.getName(), "Woodcutting", mcmmo.getConfig().getInt("Experience.Woodcutting.Dark_Oak"));
            }
        }
    }

    /**
     * check if a player is using the tree feller ability atm
     *
     * @param player the player to check
     * @return if a player is using tree feller
     */
    @Override
    public boolean treeFeller(Player player) {
        if (HookHandler.mcMMO) {
            boolean isMcMMOEnabled = Utils.plugin.getServer().getPluginManager().isPluginEnabled("mcMMO");

            if (!isMcMMOEnabled)
                return false;

            return AbilityAPI.treeFellerEnabled(player);
        }
        return false;
    }
}