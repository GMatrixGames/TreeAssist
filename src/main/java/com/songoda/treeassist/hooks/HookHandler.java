package com.songoda.treeassist.hooks;

import com.songoda.treeassist.TreeAssist;

public class HookHandler {
    public static boolean mcMMO = false;
    public static boolean jobsReborn = false;

    public static MCMMOHook mcMmoHook = new MCMMOHook();
    public static JobsRebornHook jobsHook = new JobsRebornHook();

    public static void init() {
        checkMcMMO();
        checkJobsReborn();
    }

    private static void checkMcMMO() {
        if (TreeAssist.getInstance().getConfig().getBoolean("Main.Use mcMMO if Available"))
            mcMMO = TreeAssist.getInstance().getServer().getPluginManager().isPluginEnabled("mcMMO");
    }

    private static void checkJobsReborn() {
        if (TreeAssist.getInstance().getConfig().getBoolean("Main.Use JobsReborn if Available"))
            jobsReborn = TreeAssist.getInstance().getServer().getPluginManager().isPluginEnabled("Jobs");
    }

}