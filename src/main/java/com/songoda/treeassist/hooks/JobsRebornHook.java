package com.songoda.treeassist.hooks;

import com.gamingmesh.jobs.Jobs;
import com.gamingmesh.jobs.container.Job;
import com.gamingmesh.jobs.container.JobProgression;
import com.gamingmesh.jobs.container.JobsPlayer;
import com.songoda.treeassist.TreeAssist;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

@SuppressWarnings("Duplicates")
public class JobsRebornHook implements IHook {

    @Override
    public void addExp(Player player, Block block, String species) {
        if (HookHandler.jobsReborn) {
            Job job = Jobs.getJob(TreeAssist.getInstance().getConfig().getString("Hooks.JobsReborn.JobName"));
            JobsPlayer jp = Jobs.getPlayerManager().getJobsPlayer(player);
            JobProgression jobPro = jp.getJobProgression(job);
            jobPro.addExperience(TreeAssist.getInstance().getConfig().getDouble("Hooks.JobsReborn.Experience." + species));
        }
    }

    @Override
    public void addExp(Player player, MaterialData matData, String species) {
        this.addExp(player, (Block)null, species);
    }

    @Override
    public boolean treeFeller(Player player) {
        return false;
    }
}