package com.songoda.treeassist.hooks;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public interface IHook {

    void addExp(Player player, Block block, String species);

    void addExp(Player player, MaterialData matData, String species);

    boolean treeFeller(Player player);
}