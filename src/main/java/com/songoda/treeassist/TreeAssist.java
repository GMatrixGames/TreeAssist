package com.songoda.treeassist;

import com.massivestats.MassiveStats;
import com.songoda.arconix.api.ArconixAPI;
import com.songoda.arconix.api.utils.ConfigWrapper;
import com.songoda.arconix.plugin.Arconix;
import com.songoda.treeassist.blocklists.*;
import com.songoda.treeassist.commands.*;
import com.songoda.treeassist.core.TreeBlock;
import com.songoda.treeassist.core.Utils;
import com.songoda.treeassist.hooks.HookHandler;
import com.songoda.treeassist.timers.CooldownCounter;
import com.songoda.treeassist.utils.SettingsManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

@SuppressWarnings({"WeakerAccess", "deprecation", "BukkitListenerImplemented"})
public class TreeAssist extends JavaPlugin {
    public static CommandSender console = Bukkit.getConsoleSender();

    private static TreeAssist INSTANCE;

    public boolean v1_7 = Bukkit.getServer().getClass().getPackage().getName().contains("1_7");
    public boolean v1_8 = Bukkit.getServer().getClass().getPackage().getName().contains("1_8");

    public List<Location> saplingLocationList = new ArrayList<>();
    private final Map<String, List<String>> disabledMap = new HashMap<>();
    final Map<String, AbstractCommand> commandMap = new HashMap<>();
    final List<AbstractCommand> commandList = new ArrayList<>();
    private Map<String, CooldownCounter> coolDowns = new HashMap<>();
    private Set<String> coolDownOverrides = new HashSet<>();

    public boolean isEWGEnabled;

    public ConfigWrapper langFile = new ConfigWrapper(this, "", "lang.yml");

    public SettingsManager sm;

    public References references = null;

    public ArconixAPI api;

    public boolean Enabled = true;


    public BlockList blockList;
    public TreeAssistBlockListener listener;

    public int getCoolDown(Player player) {
        if (hasCoolDown(player))
            return coolDowns.get(player.getName()).getSeconds();

        return 0;
    }

    public TreeAssistBlockListener getListener() {
        return listener;
    }

    public boolean hasCoolDown(Player player) {
        return !coolDownOverrides.contains(player.getName()) && coolDowns.containsKey(player.getName());
    }

    public boolean isActive(World world) {
        return (!getConfig().getBoolean("Worlds.Enable Per World")) ||
                getConfig().getList("Worlds.Enabled Worlds").contains(
                        world.getName());
    }

    public boolean isDisabled(String world, String player) {
        if (disabledMap.containsKey("global") && disabledMap.get("global").contains(player))
            return true;

        if (disabledMap.containsKey(world))
            return disabledMap.get(world).contains(player);

        return false;
    }

    public boolean isForceAutoDestroy() {
        return getConfig().getBoolean("Main.Automatic Tree Destruction")
                && getConfig().getBoolean("Automatic Tree Destruction.Forced Removal");
    }

    //@EventHandler
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final AbstractCommand acc = (args.length > 0) ? commandMap.get(args[0].toLowerCase()) : null;

        if (acc != null) {
            acc.commit(sender, args);
            return true;
        }
        boolean found = false;
        sender.sendMessage("");
        sender.sendMessage(api.format().formatText(references.getPrefix() + "&7" + getDescription().getVersion() + " Created by &5&l&oBrianna"));
        for (AbstractCommand command : commandList) {
            if (command.hasPerms(sender)) {
                sender.sendMessage(api.format().formatText("  &8- &a" + command.getCommandSyntax() + " &7" + command.getCommandDescription()));
                found = true;
            }
        }
        sender.sendMessage("");
        return found;
    }

    public void onDisable() {
        console.sendMessage(api.format().formatText("&a============================="));
        console.sendMessage(api.format().formatText("&7" + this.getDescription().getName() + " " + this.getDescription().getVersion() + " by &5Brianna <3&7!"));
        console.sendMessage(api.format().formatText("&7Action: &cDisabling&7..."));
        this.getServer().getScheduler().cancelTasks(this);
        if (this.blockList instanceof FlatFileBlockList) {
            blockList.save(true);
        }
        console.sendMessage(api.format().formatText("&a============================="));
    }

    public void onEnable() {
        INSTANCE = this;

        api = Arconix.pl().hook(this);

        console.sendMessage(api.format().formatText("&a============================="));
        console.sendMessage(api.format().formatText("&7" + this.getDescription().getName() + " " + this.getDescription().getVersion() + " by &5Songoda <3&7!"));
        console.sendMessage(api.format().formatText("&7Action: &aEnabling&7..."));
        sm = new SettingsManager();
        setupConfig();

        HookHandler.init();

        references = new References();

        Utils.plugin = this;
        ConfigurationSerialization.registerClass(TreeBlock.class);

        this.listener = new TreeAssistBlockListener(this);

        loadLanguageFile();

        getServer().getPluginManager().registerEvents(listener, this);
        if (getConfig().getBoolean("Main.Auto Plant Dropped Saplings"))
            getServer().getPluginManager().registerEvents(new TreeAssistSpawnListener(this), this);

        reloadLists();


        new MassiveStats(this, 900);

        initiateList("Modding.Custom Logs");
        initiateList("Modding.Custom Tree Blocks");

        if (!getConfig().getBoolean("Main.Ignore User Placed Blocks")) {
            String pluginName = getConfig().getString("Placed Blocks.Handler Plugin Name", "TreeAssist");
            switch (pluginName.toLowerCase()) {
                case "treeassist":
                    if (getConfig().getBoolean("Placed Blocks.Use FlatFile If TreeAssist"))
                        blockList = new FlatFileBlockList();
                    else
                        blockList = new EmptyBlockList();
                    break;
                case "prism":
                    blockList = new Prism2BlockList();
                    break;
                case "logblock":
                    blockList = new LogBlockBlockList();
                    break;
                case "coreprotect":
                    blockList = new CoreProtectBlockList();
                    break;
                default:
                    blockList = new EmptyBlockList();
                    break;
            }
        } else {
            //Prevent all the lovely null errors lol
            blockList = new EmptyBlockList();
        }

        blockList.initiate();

        loadCommands();

        isEWGEnabled = Utils.plugin.getServer().getPluginManager().isPluginEnabled("EpicWorldGenerator");

        if (!isEWGEnabled)
            isEWGEnabled = Utils.plugin.getServer().getPluginManager().isPluginEnabled("RealisticWorldGenerator");

        console.sendMessage(api.format().formatText("&a============================="));
    }

    public void loadLanguageFile() {
        Lang.setFile(langFile.getConfig());

        for (final Lang value : Lang.values()) {
            langFile.getConfig().addDefault(value.getNode(), value.getValue());
        }

        langFile.getConfig().options().copyDefaults(true);
        langFile.saveConfig();
    }

    private void loadCommands() {
        new CommandAddTool().load(commandList, commandMap);
        new CommandFindForest().load(commandList, commandMap);
        new CommandForceBreak().load(commandList, commandMap);
        new CommandForceGrow().load(commandList, commandMap);
        new CommandGlobal().load(commandList, commandMap);
        new CommandNoReplace().load(commandList, commandMap);
        new CommandPurge().load(commandList, commandMap);
        new CommandReload().load(commandList, commandMap);
        new CommandRemoveTool().load(commandList, commandMap);
        new CommandToggle().load(commandList, commandMap);
        new CommandTool().load(commandList, commandMap);
    }

    public void removeCountDown(String playerName) {
        try {
            coolDowns.get(playerName).cancel();
        } catch (Exception ignore) {

        }
        coolDowns.remove(playerName);
    }

    private void setupConfig() {
        sm.updateSettings();
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public void setCoolDown(Player player, GenericTree tree) {
        int coolDown = getConfig().getInt("Automatic Tree Destruction.Cooldown (seconds)", 0);
        if (coolDown == 0 || tree == null || !tree.isValid() || coolDownOverrides.contains(player.getName())) {
            return;
        } else if (coolDown < 0) {
            coolDown = tree.calculateCooldown(player.getItemInHand());
            player.sendMessage(Lang.INFO_COOLDOWN_WAIT.parse(String.valueOf(coolDown)));
        }
        CooldownCounter cc = new CooldownCounter(player, coolDown);
        cc.runTaskTimer(this, 20L, 20L);
        coolDowns.put(player.getName(), cc);
    }

    public synchronized void setCoolDownOverride(String player, boolean value) {
        if (value) {
            coolDownOverrides.add(player);
        } else {
            coolDownOverrides.remove(player);
        }
    }

    /**
     * @return true if the result is "player may use plugin"
     */
    public boolean toggleGlobal(String player) {
        return toggleWorld("global", player);
    }

    private void initiateList(String string) {
        for (Object obj : getConfig().getList(string)) {
            if (obj.equals("LIST ITEMS GO HERE")) {
                List<Object> list = new ArrayList<>();
                list.add(-1);
                getConfig().set(string, list);
                saveConfig();
                break;
            }
        }
    }

    public void reloadLists() {
    }

    /**
     * @return true if the result is "player may use plugin"
     */
    public boolean toggleWorld(String world, String player) {
        if (disabledMap.containsKey(world)) {
            if (disabledMap.get(world).contains(player)) {
                disabledMap.get(world).remove(player);
                return true;
            } else {
                disabledMap.get(world).add(player);
            }
        } else {
            disabledMap.put(world, new ArrayList<>());
            disabledMap.get(world).add(player);
        }
        return false;
    }

    public static TreeAssist getInstance() {
        return INSTANCE;
    }
}