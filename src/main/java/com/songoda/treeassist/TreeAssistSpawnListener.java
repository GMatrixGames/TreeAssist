package com.songoda.treeassist;

import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import java.util.Random;

public class TreeAssistSpawnListener implements Listener {

    public TreeAssist plugin;

    TreeAssistSpawnListener(TreeAssist instance) {
        plugin = instance;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void itemSpawnEvent(ItemSpawnEvent event) {
        Item drop = event.getEntity();
        if (!drop.getItemStack().getType().name().contains("SAPLING"))
            return;

        if ((new Random()).nextInt(100) < plugin.getConfig().getInt("Auto Plant Dropped Saplings.Chance (percent)", 10))
            new TreeAssistSaplingSelfPlant(plugin, drop);
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void playerPickupItemEvent(PlayerPickupItemEvent event) {
        Item item = event.getItem();
        if (item.getItemStack().getType().name().contains("SAPLING"))
            TreeAssistSaplingSelfPlant.remove(item);
    }
}
