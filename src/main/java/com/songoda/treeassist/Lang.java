package com.songoda.treeassist;

import com.songoda.arconix.plugin.Arconix;
import org.bukkit.configuration.file.FileConfiguration;

public enum Lang {

    ERROR_ADDTOOL_ALREADY("error.addtool.already", "&cYou have already added this as required tool!"),
    ERROR_ADDTOOL_OTHER("error.addtool.other", "&cSomething went wrong trying to add the required tool: {VAR}"),
    ERROR_CUSTOM_LISTS("error.custom.lists", "&cSomething is wrong with your custom lists. Please fix them! They need have to same item count!"),
    ERROR_CUSTOM_EXISTS("error.custom.exists", "&cThis custom block group definition already exists!"),
    ERROR_CUSTOM_EXPLANATION("error.custom.explanation", "&cYou need to have three items in you first hotbar slots. &e1) SAPLING - 2) LOG - 3) LEAVES"),
    ERROR_DATA_YML("error.data_yml", "&cYou have a messed up data.yml - fix or remove it!"),
    ERROR_EMPTY_HAND("error.emptyhand", "&cYou don't have an item in your hand"),
    ERROR_FINDFOREST("error.findforest", "&cForest not found in 500 block radius: &r{VAR}"),
    ERROR_INVALID_ARGUMENT_COUNT("error.invalid_argumentcount", "&cInvalid number of arguments&r ({VAR} instead of {VAR2})!"),
    ERROR_INVALID_ARGUMENT_LIST("error.invalid_argumentlist", "&cInvalid arguments! Valid:&r {VAR}"),
    ERROR_NOT_TOOL("error.nottool", "&cYou don't have the required tool to do that!"),
    ERROR_OUT_OF_RANGE("error.outofrange", "c&The max range for this command is: {VAR}"),
    ERROR_PERMISSION_ADDCUSTOM("error.permission.addcustom", "&cYou don't have 'treeassist.addcustom'"),
    ERROR_PERMISSION_ADDTOOL("error.permission.addtool", "&cYou don't have 'treeassist.addtool'"),
    ERROR_PERMISSION_DEBUG("error.permission.debug", "&cYou don't have 'treeassist.debug'"),
    ERROR_PERMISSION_FINDFOREST("error.permission.findforest", "&cYou don't have 'treeassist.findforest'"),
    ERROR_PERMISSION_FORCEBREAK("error.permission.forcebreak", "&cYou don't have 'treeassist.forcebreak'"),
    ERROR_PERMISSION_FORCEGROW("error.permission.forcegrow", "&cYou don't have 'treeassist.forcegrow'"),
    ERROR_PERMISSION_NOREPLACE("error.permission.noreplace", "&cYou don't have 'treeassist.noreplace'"),
    ERROR_PERMISSION_PURGE("error.permission.purge", "&cYou don't have 'treeassist.purge'"),
    ERROR_PERMISSION_RELOAD("error.permission.reload", "&cYou don't have 'treeassist.reload'"),
    ERROR_PERMISSION_REMOVECUSTOM("error.permission.removecustom", "&cYou don't have 'treeassist.removecustom'"),
    ERROR_PERMISSION_REMOVETOOL("error.permission.removetool", "&cYou don't have 'treeassist.removetool'"),
    ERROR_PERMISSION_TOGGLE("error.permission.toggle", "&cYou don't have 'treeassist.toggle'"),
    ERROR_PERMISSION_TOGGLE_OTHER("error.permission.toggle_other", "&cYou don't have 'treeassist.toggle.other'"),
    ERROR_PERMISSION_TOGGLE_GLOBAL("error.permission.toggle_global", "&cYou don't have 'treeassist.toggle.global'"),
    ERROR_PERMISSION_TOGGLE_TOOL("error.permission.toggle_tool", "&cYou don't have 'treeassist.tool'"),
    ERROR_REMOVETOOL_NOTDONE("error.removetool.not_done", "&cTool is no required tool!"),

    ERROR_NOTFOUND_WORLD("error.notfound.world", "&cWorld not found: {VAR}'"),

    ERROR_ONLY_PLAYERS("error.only.players", "Only for players!"),
    ERROR_ONLY_TREEASSIST_BLOCKLIST("error.only.treeassist_blocklist", "&cThis command only is available for the TreeAssist BlockList!"),

    INFO_PREFIX("info.prefix", "&8[&aTreeAssist&8]"),

    INFO_COOLDOWN_DONE("info.cooldown_done", "&aTreeAssist cooled down!"),
    INFO_COOLDOWN_STILL("info.cooldown_still", "&aTreeAssist is still cooling down!"),
    INFO_COOLDOWN_VALUE("info.cooldown_value", "&a{VAR} seconds remaining!"),
    INFO_COOLDOWN_WAIT("info.cooldown_wait", "&aWait {num} seconds for TreeAssist cooldown!"),

    INFO_CUSTOM_ADDED("info.custom.added", "&aCustom block group definition added!"),
    INFO_CUSTOM_REMOVED("info.custom.removed", "&aCustom block group definition removed!"),

    INFO_NEVER_BREAK_SAPLINGS("info.never_break_saplings", "&aYou cannot break saplings on this server!"),
    INFO_SAPLING_PROTECTED("info.sapling_protected", "&aThis sapling is protected!"),

    WARNING_ADDTOOL_ONLYONE("warning.sapling_protected", "&6You can only use one enchantment. Using: {VAR}"),

    SUCCESSFUL_ADDTOOL("successful.addtool", "&aRequired tool added: {VAR}"),
    SUCCESSFUL_DEBUG_ALL("successful.debug_all", "debugging EVERYTHING"),
    SUCCESSFUL_DEBUG_X("successful.debug", "debugging {VAR}"),

    SUCCESSFUL_FINDFOREST("successful.findforest", "&aForest found at &r{VAR}"),

    SUCCESSFUL_NOREPLACE("successful.noreplace", "&aYou now stop replanting trees for {VAR} seconds."),

    SUCCESSFUL_PROTECT_OFF("successful.protect_off", "&aSapling is no longer protected!"),
    SUCCESSFUL_PROTECT_ON("successful.protect_on", "&aSapling now is protected!"),

    SUCCESSFUL_PURGE_DAYS("successful.purge.days", "&a{VAR} entries have been purged for the last {VAR2} days!"),
    SUCCESSFUL_PURGE_GLOBAL("successful.purge.global", "&a{VAR} global entries have been purged!"),
    SUCCESSFUL_PURGE_WORLD("successful.purge.world", "&a{VAR} entries have been purged for the world {VAR2}!"),

    SUCCESSFUL_RELOAD("successful.reload", "&aTreeAssist has been reloaded."),

    SUCCESSFUL_REMOVETOOL("successful.removetool", "&aRequired tool removed: {VAR}"),

    SUCCESSFUL_TOGGLE_GLOBAL_OFF("successful.toggle.global_off", "&aTreeAssist functions are turned off globally!"),
    SUCCESSFUL_TOGGLE_GLOBAL_ON("successful.toggle.global_on", "&aTreeAssist functions are now turned on globally!"),

    SUCCESSFUL_TOGGLE_OTHER_OFF("successful.toggle.other_global_off", "&aTreeAssist functions are turned off for {VAR}!"),
    SUCCESSFUL_TOGGLE_OTHER_ON("successful.toggle.other_global_on", "&aTreeAssist functions are now turned on for {VAR}!"),
    SUCCESSFUL_TOGGLE_OTHER_WORLD_OFF("successful.toggle.other_world_off", "&aTreeAssist functions are turned off for {VAR} in world {VAR2}!"),
    SUCCESSFUL_TOGGLE_OTHER_WORLD_ON("successful.toggle.other_world_on", "&aTreeAssist functions are now turned on for {VAR1} in world {VAR2}!"),

    SUCCESSFUL_TOGGLE_YOU_OFF("successful.toggle.you_global_off", "&aTreeAssist functions are turned off for you!"),
    SUCCESSFUL_TOGGLE_YOU_ON("successful.toggle.you_global_on", "&aTreeAssist functions are now turned on for you!"),
    SUCCESSFUL_TOGGLE_YOU_WORLD_OFF("successful.toggle.you_world_off", "&aTreeAssist functions are turned off for you in world {VAR}!"),
    SUCCESSFUL_TOGGLE_YOU_WORLD_ON("successful.toggle.you_world_on", "&aTreeAssist functions are now turned on for you in world {VAR}!"),

    SUCCESSFUL_TOOL_OFF("successful.tool_off", "&aProtection Tool removed!"),
    SUCCESSFUL_TOOL_ON("successful.tool_on", "&aYou have been given the Protection Tool!");


    private final String node;
    private String value;
    private static FileConfiguration LANG;

    public static void setFile(final FileConfiguration config) {
        LANG = config;
    }

    Lang(final String node, final String value) {
        this.node = node;
        this.value = value;
    }

    public String getNode() {
        return node;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String sValue) {
        value = sValue;
    }

    public String parse() {
        return parse(null);
    }

    public String parse(int arg) {
        return parse(String.valueOf(arg));
    }

    public String parse(int arg, String arg2) {
        return parse(String.valueOf(arg), arg2);
    }

    public String parse(String arg) {
        return parse(arg, null);
    }

    public String parse(String arg, String arg2) {

        if (arg != null)
            value = value.replace("{VAR}", arg);

        if (arg2 != null)
            value = value.replace("{VAR2}", arg2);

        return Arconix.pl().getApi().format().formatText(value);
    }


    @Override
    public String toString() {
        return value;
    }
}
