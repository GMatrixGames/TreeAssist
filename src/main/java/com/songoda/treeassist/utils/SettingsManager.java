package com.songoda.treeassist.utils;

import com.songoda.arconix.plugin.Arconix;
import com.songoda.treeassist.TreeAssist;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by songoda on 6/4/2017.
 */
@SuppressWarnings("deprecation")
public class SettingsManager implements Listener {

    private Map<Player, Integer> page = new HashMap<>();

    private static ConfigWrapper defs;

    public SettingsManager() {
        TreeAssist.getInstance().saveResource("SettingDefinitions.yml", true);
        defs = new ConfigWrapper(TreeAssist.getInstance(), "", "SettingDefinitions.yml");
        defs.createNewFile("Loading data file", "TreeAssist SettingDefinitions file");
        TreeAssist.getInstance().getServer().getPluginManager().registerEvents(this, TreeAssist.getInstance());
    }

    private Map<Player, String> current = new HashMap<>();

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getInventory() != null) {
            if (e.getInventory().getTitle().equals("TreeAssist Settings Editor")) {
                Player p = (Player) e.getWhoClicked();
                if (e.getCurrentItem().getType().name().contains("STAINED_GLASS")) {
                    e.setCancelled(true);
                } else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("Next")) {
                    page.put(p, 2);
                    openEditor(p);
                } else if (e.getCurrentItem().getItemMeta().getDisplayName().equals("back")) {
                    page.put(p, 1);
                    openEditor(p);
                } else if (e.getCurrentItem() != null) {
                    e.setCancelled(true);

                    String key = e.getCurrentItem().getItemMeta().getDisplayName().substring(2);

                    if (TreeAssist.getInstance().getConfig().get("settings." + key).getClass().getName().equals("java.lang.Boolean")) {
                        boolean bool = (Boolean) TreeAssist.getInstance().getConfig().get("settings." + key);
                        if (!bool)
                            TreeAssist.getInstance().getConfig().set("settings." + key, true);
                        else
                            TreeAssist.getInstance().getConfig().set("settings." + key, false);
                        finishEditing(p);
                    } else {
                        editObject(p, key);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        final Player p = e.getPlayer();
        if (current.containsKey(p)) {
            switch (TreeAssist.getInstance().getConfig().get("settings." + current.get(p)).getClass().getName()) {
                case "java.lang.Integer":
                    TreeAssist.getInstance().getConfig().set("settings." + current.get(p), Integer.parseInt(e.getMessage()));
                    break;
                case "java.lang.Double":
                    TreeAssist.getInstance().getConfig().set("settings." + current.get(p), Double.parseDouble(e.getMessage()));
                    break;
                case "java.lang.String":
                    TreeAssist.getInstance().getConfig().set("settings." + current.get(p), e.getMessage());
                    break;
            }
            finishEditing(p);
            e.setCancelled(true);
        }
    }

    private void finishEditing(Player p) {
        current.remove(p);
        TreeAssist.getInstance().saveConfig();
        openEditor(p);
    }


    private void editObject(Player p, String current) {
        this.current.put(p, current);
        p.closeInventory();
        p.sendMessage("");
        p.sendMessage(Arconix.pl().getApi().format().formatText("&7Please enter a value for &6" + current + "&7."));
        if (TreeAssist.getInstance().getConfig().get("settings." + current).getClass().getName().equals("java.lang.Integer")) {
            p.sendMessage(Arconix.pl().getApi().format().formatText("&cUse only numbers."));
        }
        p.sendMessage("");
    }

    private void openEditor(Player p) {
        int pmin = 1;

        if (page.containsKey(p))
            pmin = page.get(p);

        if (pmin != 1)
            pmin = 45;

        int pmax = pmin * 44;

        Inventory i = Bukkit.createInventory(null, 54, "TreeAssist Settings Editor");

        int num = 0;
        int total = 0;
        ConfigurationSection cs = TreeAssist.getInstance().getConfig().getConfigurationSection("settings");
        for (String key : cs.getKeys(true)) {
            if (!key.contains("Entities")) {
                if (total >= pmin - 1 && total <= pmax) {

                    ItemStack item = new ItemStack(Material.DIAMOND_HELMET);
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName(Arconix.pl().getApi().format().formatText("&6" + key));
                    ArrayList<String> lore = new ArrayList<>();
                    switch (TreeAssist.getInstance().getConfig().get("settings." + key).getClass().getName()) {
                        case "java.lang.Boolean":

                            item.setType(Material.LEVER);
                            boolean bool = (Boolean) TreeAssist.getInstance().getConfig().get("settings." + key);

                            if (!bool)
                                lore.add(Arconix.pl().getApi().format().formatText("&c" + false));
                            else
                                lore.add(Arconix.pl().getApi().format().formatText("&a" + true));

                            break;
                        case "java.lang.String":
                            item.setType(Material.PAPER);
                            String str = (String) TreeAssist.getInstance().getConfig().get("settings." + key);
                            lore.add(Arconix.pl().getApi().format().formatText("&9" + str));
                            break;
                        case "java.lang.Integer":
                            item.setType(Material.CLOCK);

                            int in = (Integer) TreeAssist.getInstance().getConfig().get("settings." + key);
                            lore.add(Arconix.pl().getApi().format().formatText("&5" + in));
                            break;
                    }
                    if (defs.getConfig().contains(key)) {
                        String text = defs.getConfig().getString(key);

                        Pattern regex = Pattern.compile("(.{1,28}(?:\\s|$))|(.{0,28})", Pattern.DOTALL);
                        Matcher m = regex.matcher(text);
                        while (m.find()) {
                            if (m.end() != text.length() || m.group().length() != 0)
                                lore.add(Arconix.pl().getApi().format().formatText("&7" + m.group()));
                        }
                    }
                    meta.setLore(lore);
                    item.setItemMeta(meta);

                    i.setItem(num, item);
                    num++;
                }
                total++;
            }
        }


        int nu = 45;
        while (nu != 54) {
            i.setItem(nu, new ItemStack(Material.BLACK_STAINED_GLASS_PANE));
            nu++;
        }


        ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (byte) 3);
        ItemStack skull = head;
        if (!TreeAssist.getInstance().v1_7)
            skull = Arconix.pl().getApi().getGUI().addTexture(head, "http://textures.minecraft.net/texture/1b6f1a25b6bc199946472aedb370522584ff6f4e83221e5946bd2e41b5ca13b");
        SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
        if (TreeAssist.getInstance().v1_7)
            skullMeta.setOwner("MHF_ArrowRight");
        skull.setDurability((short) 3);
        skullMeta.setDisplayName("Next");
        skull.setItemMeta(skullMeta);

        ItemStack head2 = new ItemStack(Material.PLAYER_HEAD, 1, (byte) 3);
        ItemStack skull2 = head2;
        if (!TreeAssist.getInstance().v1_7)
            skull2 = Arconix.pl().getApi().getGUI().addTexture(head2, "http://textures.minecraft.net/texture/3ebf907494a935e955bfcadab81beafb90fb9be49c7026ba97d798d5f1a23");
        SkullMeta skull2Meta = (SkullMeta) skull2.getItemMeta();
        if (TreeAssist.getInstance().v1_7)
            skull2Meta.setOwner("MHF_ArrowLeft");
        skull2.setDurability((short) 3);
        skull2Meta.setDisplayName("Back");
        skull2.setItemMeta(skull2Meta);

        if (pmin != 1) {
            i.setItem(46, skull2);
        }
        if (pmin == 1) {
            i.setItem(52, skull);
        }

        p.openInventory(i);
    }


    public void updateSettings() {
        for (settings s : settings.values()) {
            TreeAssist.getInstance().getConfig().addDefault(s.setting, s.option);
        }
    }

    public static boolean contains(String test) {
        for (settings c : settings.values()) {
            if (c.setting.equals(test)) {
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("unused")
    public enum settings {

        o1("Main.Apply Full Tool Damage", true),
        o2("Main.Auto Add To Inventory", false),
        o222("Main.Play Sound On Chop", true),
        o3("Main.Auto Plant Dropped Saplings", false),
        o4("Main.Automatic Tree Destruction", true),
        o5("Main.Destroy Only Blocks Above", false),
        o6("Main.Force Break Default Radius", 10),
        o7("Main.Force Grow Default Radius", 10),
        o8("Main.Force Break Max Radius", 30),
        o9("Main.Force Grow Max Radius", 30),
        o10("Main.Ignore User Placed Blocks", false),
        o11("Main.Initial Delay", false),
        o12("Main.Language", "en"),
        o13("Main.Sapling Replant", true),
        o14("Main.Toggle Default", true),
        o15("Main.Use mcMMO if Available", true),
        o75("Main.Use JobsReborn if Available", true),
        o16("Main.Use Permissions", false),


        o17("Automatic Tree Destruction.Required Lore", ""),
        o188("Automatic Tree Destruction.Animated", true),
        o18("Automatic Tree Destruction.When Sneaking", true),
        o19("Automatic Tree Destruction.When Not Sneaking", true),
        o20("Automatic Tree Destruction.Forced Removal", false),
        o21("Automatic Tree Destruction.Remove Leaves", true),
        o22("Automatic Tree Destruction.Initial Delay (seconds)", 10),
        o23("Automatic Tree Destruction.Delay (ticks)", 0),

        o24("Automatic Tree Destruction.Tree Types.Birch", true),
        o25("Automatic Tree Destruction.Tree Types.Jungle", true),
        o28("Automatic Tree Destruction.Tree Types.Oak", true),
        o29("Automatic Tree Destruction.Tree Types.Spruce", true),
        o30("Automatic Tree Destruction.Tree Types.Brown Shroom", true),
        o31("Automatic Tree Destruction.Tree Types.Red Shroom", true),
        o32("Automatic Tree Destruction.Tree Types.Acacia", true),
        o33("Automatic Tree Destruction.Tree Types.Dark Oak", true),

        o34("Automatic Tree Destruction.Cooldown (seconds)", 0),

        o35("Auto Plant Dropped Saplings.Chance (percent)", 10),
        o36("Auto Plant Dropped Saplings.Delay (seconds)", 5),

        o37("Leaf Decay.Fast Leaf Decay", true),

        o38("Sapling Replant.Command Time Delay (Seconds)", 30),
        o39("Sapling Replant.Bottom Block has to be Broken First", true),
        o40("Sapling Replant.Time to Protect Sapling (Seconds)", 0),
        o41("Sapling Replant.Time to Block Sapling Growth (Seconds)", 0),
        o42("Sapling Replant.Replant When Tree Burns Down", true),
        o43("Sapling Replant.Block all breaking of Saplings", false),
        o44("Sapling Replant.Delay until Sapling is replanted (seconds) (minimum 1 second)", 1),
        o45("Sapling Replant.Enforce", false),

        o46("Sapling Replant.Tree Types to Replant.Birch", true),
        o47("Sapling Replant.Tree Types to Replant.Jungle", true),
        o50("Sapling Replant.Tree Types to Replant.Oak", true),
        o51("Sapling Replant.Tree Types to Replant.Spruce", true),
        o52("Sapling Replant.Tree Types to Replant.Brown Shroom", true),
        o53("Sapling Replant.Tree Types to Replant.Red Shroom", true),
        o54("Sapling Replant.Tree Types to Replant.Acacia", true),
        o55("Sapling Replant.Tree Types to Replant.Dark Oak", true),

        o56("Tools.Sapling Replant Require Tools", true),
        o57("Tools.Tree Destruction Require Tools", true),
        o58("Tools.Tools List", Arrays.asList("DIAMOND_AXE", "WOODEN_AXE", "GOLDEN_AXE", "IRON_AXE", "STONE_AXE")),
        o59("Tools.Drop Chance.DIAMOND_AXE", 100),
        o60("Tools.Drop Chance.WOODEN_AXE", 100),
        o61("Tools.Drop Chance.GOLDEN_AXE", 100),
        o62("Tools.Drop Chance.IRON_AXE", 100),
        o63("Tools.Drop Chance.STONE_AXE", 100),

        o64("Worlds.Enable Per World", false),
        o65("Worlds.Enabled Worlds", Arrays.asList("world", "world2")),

        o66("Custom Drops.APPLE", 0.01),
        o67("Custom Drops.GOLDEN_APPLE", 0.0),

        o68("Placed Blocks.Handler Plugin Name", "TreeAssist"),
        o69("Placed Blocks.Use FlatFile If TreeAssist", false),

        o70("Modding.Disable Durability Fix", false),

        o71("Modding.Custom Logs", Collections.singletonList(-1)),
        o72("Modding.Custom Tree Blocks", Collections.singletonList(-1)),
        o73("Modding.Custom Saplings", Collections.singletonList(-1)),
        o74("Modding.Debug", "none"),

        o76("Hooks.JobsReborn.JobName", "Woodcutter"),
        o77("Hooks.JobsReborn.Experience.Birch", 2.5),
        o78("Hooks.JobsReborn.Experience.Jungle", 2.5),
        o79("Hooks.JobsReborn.Experience.Oak", 2.5),
        o80("Hooks.JobsReborn.Experience.Spruce", 2.0),
        o81("Hooks.JobsReborn.Experience.Brown Shroom", 3.0),
        o82("Hooks.JobsReborn.Experience.Red Shroom", 3.0),
        o83("Hooks.JobsReborn.Experience.Acacia", 2.5),
        o84("Hooks.JobsReborn.Experience.Dark Oak", 2.5);


        private String setting;
        private Object option;

        settings(String setting, Object option) {
            this.setting = setting;
            this.option = option;
        }

    }
}
